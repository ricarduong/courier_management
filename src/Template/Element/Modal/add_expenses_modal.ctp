<div id="add-expenses-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="data-modal-expenses modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <br>
        <div class="alert alert-danger alert-message hidden" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            Thêm thất bại
        </div>
        <h4 id="login-modalLabel" class="modal-title"><?= $title = ($IE == 'E') ? 'Khoản chi' : 'Khoản thu' ?></h4>
      </div>
      <div class="modal-body add_expenses_modal" style="height: auto;">
            <?php 
                echo $this->Form->create('addEmployee',
                    [   
                        'class' => 'form-horizontal',
                        'id' => 'form-data-expense'
                    ]
                );
            ?>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Tên <?= $name = ($IE == 'E') ? 'Khoản chi' : 'Khoản thu' ?></label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('name',
                            [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => true,
                                'data-name-expenses'
                            ]
                        )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Ghi chú</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('note',
                            [
                                'class' => 'form-control',
                                'label' => false
                            ]
                        )
                    ?>
                    <?=
                        $this->Form->control('type',
                            [
                                'class' => 'form-control hidden',
                                'label' => false,
                                'value' => $IE
                            ]
                        )
                    ?>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <?=
            $this->Form->button('Tạo mới', [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'btn-add-expense'
            ]);
        ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>