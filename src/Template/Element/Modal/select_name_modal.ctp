<div id="select-name-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="data-modal-ope modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <h4 id="login-modalLabel" class="modal-title">Danh sách đối tượng</h4>
            <div class="row" style="margin-top: 10px;">
                <?=
                    $this->Form->create('addEmployee', [
                        'class' => 'form-horizontal',
                        'id' => 'form-data-ope'
                    ]);
                ?>
                <div class="col-lg-8 col-md-8">
                    <?=
                        $this->Form->control('name', [
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
                <div class="col-lg-4 col-md-4">
                    <?=
                        $this->Form->button('search', [
                                'type' => 'button',
                                'class' => 'btn btn-primary',
                                'btn-ope-search'
                            ]
                        );
                    ?>
                </div>
                <?= $this->Form->end(); ?>
            </div>
            <br>
                <div class="alert alert-danger alert-message hidden" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Không tìm thấy dữ liệu tương ứng
            </div>
      </div>
      <div class="modal-body" style="height: 450px;">
            <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all" table-before-search>
                <thead>
                    <tr>
                        <th></th>
                        <th align="center">Loại</th>
                        <th>Mã đối tượng</th>
                        <th>Tên đối tượng</th>
                        <th>Đơn vị</th>
                        <th>Điện thoại</th>
                        <th>Địa chỉ</th>
                    </tr>
                </thead>
                <tbody class="list-operator-before-search">
                    <?php
                    if (!empty($data_operator)):
                        foreach ($data_operator as $key => $value):
                    ?>
                    <tr>
                        <th><?= $this->Form->radio('id',
                            [
                                'label' => false
                            ],
                            [
                                'data-name-ope' => $value['operator_name'],
                                'data-address-ope' => $value['address'],
                                'data-id-ope' => $value['id_operator'],
                                'data-value-ope'
                            ]
                        ); ?></th>
                        <td><?= $value['operator_type']['type_name_ope'] ?></td>
                        <td><?= $value['operator_code'] ?></td>
                        <td><?= $value['operator_name'] ?></td>
                        <td><?= $value['unit'] ?></td>
                        <td><?= $value['phone'] ?></td>
                        <td><?= $value['address'] ?></td>
                    </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </tbody>
                 
            </table>
      </div>
      <div class="modal-footer">
        <?=
            $this->Form->button('OK', [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'btn-submit-ope',
                'data-dismiss' => "modal"
            ]);
        ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>