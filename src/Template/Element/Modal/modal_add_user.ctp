<div id="modal-add-user" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <h4 id="login-modalLabel" class="modal-title">Add User</h4>
      </div>
      <div class="modal-body">
                  <!-- Form login -->
                <?= $this->Form->create('null', [ 'url' => ['controller' => 'Users', 'action' => 'add']]); ?>
                    <div class="form-group">
                        <?= $this->Form->control('username', ['label' => __('label_login_username'), 'class' => 'form-control', 'autofocus' => 'true', 'required' => 'required']) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('password', ['label' => __('label_password'), 'class' => 'form-control', 'type' => 'password',]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('phone', ['label' => __('Phone'), 'class' => 'form-control', 'required' => 'required']) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('role', ['options' => ['0' => 'Admin', '1' => 'Staff'], 
                                                'label' => __('label_role'), 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                      <?php
                      if(!empty($data_authorities)):
                        foreach ($data_authorities as $authorities):
                          $options = [
            '1' => 'Mitglied',
            '2' => 'Interessent',
            '3' => 'Ehemaliger',
            '4' => 'Bewohner',
            '5' => 'Partner',
            '6' => 'Warteliste',
            '7' => 'Gärtner',
        ];
                      ?>
                        <?= $this->Form->checkbox('authorities_id[]', ['hiddenField' => false ,'class' => 'ai', 'id' => $authorities->id , 'value' => $authorities->id]); ?>
                        <label for=<?= $authorities->id ?>><?= $authorities->name ?></label>
                        
                      <?php
                        endforeach;
                      endif;
                      ?>
                      <?= $this->Form->select('member', $options, ['multiple' => 'checkbox']); ?>
                      <?= $this->Form->control('member', array('label'  => false,'type' => 'select',
                                                'multiple'=>'checkbox',
                                                'options' => $options,
                                                )); ?>
                      <?= $this->Form->select('select', $options, [
  'label'    => false,
  'multiple' => true,
]) ?>
                    </div>
                    <button type="submit" class="btn btn-primary"><?= __('label_button_add_user')?></button>
                    <div class="clear"></div>
                <?= $this->Form->end() ?>
                </form>
                <!-- End form add-->
      </div>
    </div>
  </div>
</div>