<div id="add-name-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" data-modal-operator class="modal data-modal-operator fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <br>
        <div class="alert alert-danger alert-message hidden" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            Thêm thất bại 
        </div>
        <h4 id="login-modalLabel" class="modal-title">Đối tượng thu/chi</h4>

      </div>
      <div class="modal-body add_name_modal" style="height: 450px;">
            <?=
                $this->Form->create('addEmployee', [
                    'class' => 'form-horizontal',
                    'id' => 'form-data'
                ]);
            ?>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Mã đối tượng</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('operator_code', [
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'data-ope-code' => 'ope-code'
                            ]
                        );
                    ?>
                    <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Tên đối tượng</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('operator_name', [
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'data-ope-name' => 'ope-name'
                            ]
                        );
                    ?>
                    <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Đơn vị</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('unit', [
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Chức vụ</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('position', [
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Địa chỉ</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('address', [
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'data-ope-address' => 'ope-address'
                            ]
                        );
                    ?>
                    <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Điện thoại</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('phone', [
                                'type' => 'number',
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Fax</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('fax', [
                                'type' => 'number',
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Website</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('website', [
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Email</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('email', [
                                'type' => 'email',
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Số tài khoản</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('account_num', [
                                'type' => 'number',
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Ngân hàng</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('bank', [
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Loại</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->select('id_type',
                            $data_type,
                            [
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => "Chọn loại",
                                'required' => true,
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Ghi chú</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('note', [
                                'type' => 'textarea',
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Mã số thuế</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('tax_code', [
                                'type' => 'number',
                                'label' => false,
                                'class' => 'form-control',
                            ]
                        );
                    ?>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <?=
            $this->Form->button('Tạo mới', [
                    'class' => 'btn btn-primary',
                    'type' => 'button',
                    'btn-add-ope'
                ]
            );
        ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>