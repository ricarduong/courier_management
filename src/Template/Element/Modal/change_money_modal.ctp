<div id="change-money-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="data-modal-currency modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <h4 id="login-modalLabel" class="modal-title">Danh sách ngoại tệ</h4>
      </div>
      <div class="modal-body" style="height: 450px;">
            <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all">
                   <?php 
                        echo $this->Form->create('addEmployee',
                            [   
                                'class' => 'form-horizontal',
                                'id' => 'form-data-currency'
                            ]
                        );
                    ?>
                <thead>
                    <tr>
                        <th>Loại tiền</th>
                        <th align="center">Tỷ giá (VND)</th>
                        <th></th>
                    </tr>
                </thead>
                 <?php
                    if (!empty($currency)):
                        foreach ($currency as $key => $value):
                ?>
                <tr>
                    <td>
                        <?=
                            $this->Form->control('id_currency',
                                [
                                    'class' => 'form-control hidden',
                                    'label' => false,
                                    'value' => $value['id_currency']
                                ]
                            )
                        ?>
                        <?=
                            $this->Form->control('currency_name',
                                [
                                    'class' => 'form-control',
                                    'label' => false,
                                    'value' => $value['currency_name']
                                ]
                            )
                        ?>
                    </td>
                    <td number-input>
                        <?=
                            $this->Form->control('exchange_rate',
                                [
                                    'class' => 'form-control',
                                    'label' => false,
                                    'value' => number_format($value['exchange_rate']),
                                    'data-exchange-rate'
                                ]
                            )
                        ?>
                    </td>
                    <td>
                        <?=
                            $this->Form->button('Đổi', [
                                'type' => 'button',
                                'class' => 'btn btn-primary',
                                'btn-edit-currency'
                            ]);
                        ?>
                    </td>
                </tr>
                <?php
                        endforeach;
                    endif;
                ?>
                <?= $this->Form->end(); ?>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>