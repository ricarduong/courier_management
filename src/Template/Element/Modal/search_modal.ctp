<div id="search-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="login-modalLabel" class="modal-title"><?= __('nav_top_search') ?></h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
      </div>
      <div class="modal-body search_modal">
            <input id="code_modal" type="text" placeholder="<?= __('nav_top_search_input') ?>" class="form-control">
            <br>
            <button type="" id="submit_code_modal" class="btn btn-template-outlined"><?= __('nav_top_search_submit') ?></button>
            <div id="info_search_modal">
            </div>
      </div>
    </div>
  </div>
</div>