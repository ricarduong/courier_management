<div id="add-shipper-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="data-modal-shipper modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <br>
        <div class="alert alert-danger alert-message hidden" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            Thêm thất bại
        </div>
        <h4 id="login-modalLabel" class="modal-title">Shipper</h4>
      </div>
      <div class="modal-body add_shipper_modal" style="height: auto;">
            <?php 
                echo $this->Form->create('addEmployee',
                    [   
                        'class' => 'form-horizontal',
                        'id' => 'form-data-shipper'
                    ]
                );
            ?>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Name</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('shipper_name',
                            [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => true,
                                'data-name-shipper'
                            ]
                        )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Address</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('address',
                            [
                                'class' => 'form-control',
                                'label' => false,
                                'address-shipper'
                            ]
                        )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12">Phone</label>
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <?=
                        $this->Form->control('phone',
                            [
                                'class' => 'form-control',
                                'label' => false,
                                'phone-shipper'
                            ]
                        )
                    ?>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <?=
            $this->Form->button('Tạo mới', [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'btn-add-shipper'
            ]);
        ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>