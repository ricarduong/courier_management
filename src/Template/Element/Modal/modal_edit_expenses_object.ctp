
<div id="modal-edit-expenses-object" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" data-modal-operator class="modal data-modal-operator fade">
  <div role="document" class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <br>
        <div class="alert alert-danger alert-message hidden" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            Cập nhật thất bại
        </div>
        <h3 id="login-modalLabel" class="modal-title">Đối tượng thu/chi</h3>

      </div>
      <div class="modal-body modal-edit-expenses-object">
            <?=
                $this->Form->create('addEmployee', [
                    'class' => 'form-horizontal',
                    'id' => 'form-data'
                ]);
            ?>
            <div class="form-group">
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Mã đối tượng</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('id', [
                                'label' => false,
                                'class' => 'form-control hidden',
                                'id-ope-edit'
                            ]
                        );
                    ?>
                    <?=
                        $this->Form->control('operator_code', [
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'data-ope-code' => 'ope-code'
                            ]
                        );
                    ?>
                    <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                </div>
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Tên đối tượng</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('operator_name', [
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'data-ope-name' => 'ope-name'
                            ]
                        );
                    ?>
                    <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                </div>
            </div>
            <!-- <div class="form-group">

            </div> -->
            <div class="form-group">
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Đơn vị</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('unit', [
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-unit' => 'ope-unit'
                            ]
                        );
                    ?>
                </div>
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Chức vụ</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('position', [
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-position' => 'ope-position'
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Địa chỉ</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('address', [
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'data-ope-address' => 'ope-address'
                            ]
                        );
                    ?>
                    <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                </div>
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Điện thoại</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('phone', [
                                'type' => 'tel',
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-phone' => 'ope-phone'
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Fax</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('fax', [
                                'type' => 'tel',
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-fax' => 'ope-fax'
                            ]
                        );
                    ?>
                </div>
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Website</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('website', [
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-website' => 'ope-website'
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Email</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('email', [
                                'type' => 'email',
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-email' => 'ope-email'
                            ]
                        );
                    ?>
                </div>
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Số tài khoản</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('account_num', [
                                'type' => 'tel',
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-account-num' => 'ope-account-num'
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Ngân hàng</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('bank', [
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-bank' => 'ope-bank'
                            ]
                        );
                    ?>
                </div>
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Loại</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->select('id_type',
                            $data_type,
                            [
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => "Chọn loại",
                                'required' => true,
                                'data-ope-id-type' => 'ope-id-type'
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Mã số thuế</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('tax_code', [
                                'type' => 'tel',
                                'label' => false,
                                'class' => 'form-control',
                                'data-ope-tax-code' => 'ope-tax-code'
                            ]
                        );
                    ?>
                </div>
                <label class="control-label col-lg-2 col-md-2 col-sm-12">Ghi chú</label>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <?=
                        $this->Form->control('note', [
                                'type' => 'textarea',
                                'label' => false,
                                'class' => 'form-control',
                                'rows' => 3,
                                'data-ope-note' => 'ope-note'
                            ]
                        );
                    ?>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <?=
            $this->Form->button('Cập nhật', [
                    'class' => 'btn btn-primary',
                    'type' => 'button',
                    'btn-add-ope'
                ]
            );
        ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>
