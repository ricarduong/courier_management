<div id="modal-edit-authorities" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
        <h4 id="login-modalLabel" class="modal-title">Sửa thông tin quyền</h4>
      </div>
      <div class="modal-body">
          <!-- Form login -->
        <?= $this->Form->create('null', [ 'url' => ['controller' => 'Users', 'action' => 'editAuthorities']]); ?>
            <div class="form-group">
                <?= $this->Form->control('name', ['label' => __('label_authorities_name'), 'value' => $value['name'], 'class' => 'form-control', 'autofocus' => 'true', 'required' => 'required']) ?>
                <?= $this->Form->control('id', ['value' => $value['id'], 'type' => 'hidden']) ?>
            </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('label_modal_button_close')?></button>
        <button type="submit" id="button" class="btn btn-primary" data-url=""><?= __('label_button_add_user')?></button>
      </div>
      <div class="clear"></div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>