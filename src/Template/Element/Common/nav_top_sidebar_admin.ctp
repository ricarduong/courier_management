<nav class="navbar navbar-default navbar-static-top navbar-fixed-top" role="navigation" style="margin-bottom: 50px">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Quản lý thu chi</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                    </ul>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/users/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Trang Chủ</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Quản lý người dùng<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Thông tin người dùng',
                                            [
                                                'controller' => 'users',
                                                'action' => 'index',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Quản lý quyền',
                                            [
                                                'controller' => 'users',
                                                'action' => 'authorities',
                                            ]
                                        );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Thông tin website<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Dịch vụ',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'webServices',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Thông tin Công Ty',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'webCompanyInformation',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Liên hệ',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'webContact',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Hỗ trợ khách hàng',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'webSupportCustomer',
                                            ]
                                        );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Khách hàng',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manage',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Các khoản chi',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manageExpenses',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Các khoản thu',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manageRevenues',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Đối tượng thu/chi',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manageExpensesObject',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Phiếu chi',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'managePaymentSlip',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Phiếu thu',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manageReceipt',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Công nợ',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manageMortgage',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Quỹ / Tài khoản',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manageFund',
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Tiền tệ',
                                            [
                                                'controller' => 'admin',
                                                'action' => 'manageMoney',
                                            ]
                                        );
                                    ?>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="/admin/tables"><i class="fa fa-table fa-fw"></i>Thống kê</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Thiết lập</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>