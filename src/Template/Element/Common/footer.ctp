<footer class="main-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <h4 class="h6"><?= __('nav_top_about_us') ?></h4>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
          
          <hr class="d-block d-lg-none">
        </div>
       
        <div class="col-lg-3">
          <h4 class="h6"><?= __('nav_top_contact') ?></h4>
          <p class="text-uppercase"><strong>Universal Ltd.</strong><br>13/25 New Avenue <br>Newtown upon River <br>45Y 73J <br>England <br><strong>Great Britain</strong></p>
          <hr class="d-block d-lg-none">
        </div>
        <div class="col-lg-6">
          <h4 class="h6"><?= __('nav_top_maps') ?></h4>
        </div>
      </div>
    </div>
    <div class="copyrights">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 text-center-md">
            <p>&copy; 2018. Your company / name goes here</p>
          </div>
          <div class="col-lg-8 text-right text-center-md">
            <p>Design by <a href="#">PHPCodeDao</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
  </footer>