<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h4>Quản lý Quỹ / Tài khoản</h4>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default fund-and-account">
            <div class="panel-heading">
                <b>Chi tiết Quỹ / Tài khoản</b>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('addFund',
                         [  
                            'class' => 'form-vertical',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->control('fund_name', [
                                                'type' => 'text',
                                                'label' => 'Tên Quỹ / Tài khoản',
                                                'class' => 'form-control',
                                                'required' => true,
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Loại tiền</label>
                                    <?=
                                        $this->Form->select('id_money', 
                                            $data_money,
                                            [
                                                'label' => 'Loại tiền',
                                                'class' => 'form-control',
                                                'empty' => 'Chon loai',
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group" number-input>
                                    <?=
                                        $this->Form->control('original_amount',
                                            [
                                                'label' => 'Số tiền ban đầu',
                                                'class' => 'form-control',
                                                'type' => 'text'
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group" number-input>
                                   <?=
                                        $this->Form->control('collected',
                                            [
                                                'type' => 'text',
                                                'label' => 'Đã thu',
                                                'class' => 'form-control',
                                                'empty' => true,
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group" number-input>
                                    <?=
                                        $this->Form->control('spent', [
                                                'type' => 'text',
                                                'label' => 'Đã chi',
                                                'class' => 'form-control',
                                                'required' => true,
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->control('note', [
                                                'label' => 'Ghi chú',
                                                'class' => 'form-control',
                                                'type' => 'textarea',
                                                'rows' => 3
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <label class="hidden-xs hidden-sm"></label>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->button('Thêm', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-primary pull-right'
                                        ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                DANH SÁCH QUỸ / TÀI KHOẢN
            </div>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all">
                    <thead>
                        <tr>
                            <th>Tên Quỹ/Tài khoản</th>
                            <th>Loại tiền</th>
                            <th>Số tiền ban đầu</th>
                            <th>Đã thu</th>
                            <th>Đã chi</th>
                            <th>Còn lại</th>
                            <th>Ghi chú</th>
                        </tr>
                    </thead>
                    <?php
                        if (!empty($data_fund)):
                            foreach ($data_fund as $key => $value):
                    ?>
                        <tr>
                            <td><?= $value['fund_name'] ?></td>
                            <td><?= $value['money']['money_name'] ?></td>
                            <td><?= number_format($value['original_amount']) ?></td>
                            <td><?= number_format($value['collected']) ?></td>
                            <td><?= number_format($value['spent']) ?></td>
                            <td><?= number_format($value['remain']) ?></td>
                            <td><?= $value['note'] ?></td>
                        </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>