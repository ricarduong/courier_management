<?= $this->Html->script(array(
        'ckeditor/ckeditor.js',
    )) 
?>
<div class="row ">
    <div class="page-header-top">
      <div class="col-lg-12 col-md-12">
          <h4>Thêm dịch vụ</h4>
          <hr>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
      <div class="panel panel-default">
          <div class="title chonngonngu">
              <ul class="nav nav-tabs nav-justified">
              <?php 
                 if(!empty($langsetlang)) :
                      foreach ($langsetlang as $lang =>$valuelang):
                          if(!empty($valuelang)) :
              ?>
                              <li class="<?= $valuelang['type']=='vi_VN'?'active':'' ?>" data-lang="<?=$valuelang['type']?>">
                              <a href="" title="Chọn <?=$valuelang['name']?> ">
                              <?=$valuelang['name']?>
                              </a>
                            </li>
              <?php
                      endif;
                    endforeach;
                endif;
              ?>
              </ul>
          </div>
          <div class="panel-body">    
            <?= 
              $this->Form->create();
            ?>
              <div class="row">
                  <!-- ====================type (VD: services, article, product...)=================== -->
                    <?= $this->Form->control('type', 
                                            [
                                              'type' => 'hidden', 
                                              'value'=> 'servieces',
                                            ]) 
                    ?>          
                    <!-- ================ Name ================== -->
                    <?php        
                     if(!empty($langsetlang)) :
                          foreach ($langsetlang as $lang =>$valuelang):
                              if(!empty($valuelang)) :
                                ?>
                                <!-- <input type="text" name="a"> -->
                                <div id="" class="form-group col-sm-6 lang_hidden lang_<?=$valuelang['type']?> <?=$valuelang['type']=='vi_VN'?'active':''?>">
                                    <?= $this->Form->control('name_'.$valuelang["type"], 
                                        [
                                          'label' => 'Tiêu đề ('.$valuelang["name"].')', 
                                          'type' => 'text', 
                                          'maxlength' => '100',
                                          'class' => 'form-control ckeditor',
                                        ])
                                    ?>  
                                </div>
                                <?php
                          endif;
                        endforeach;
                    endif
                  ?>
                  <!-- ===============description services================= -->
                  <?php
                  if(!empty($langsetlang)) :
                          foreach ($langsetlang as $lang =>$valuelang):
                              if(!empty($valuelang)) :
                                ?>
                                <!-- <input type="text" name="a"> -->
                                <div class="form-group col-sm-6 lang_hidden lang_<?=$valuelang['type']?> <?=$valuelang['type']=='vi_VN'?'active':''?>">
                                  <!-- <label for="inputnametitle" class="col-sm-2 control-label">Tiêu đề (<?=$valuelang["name"]?>):</label> -->
                                    <?= $this->Form->control('description_'.$valuelang["type"], 
                                        [
                                          'label' => 'Mô tả ('.$valuelang["name"].')', 
                                          'type' => 'text', 
                                          'maxlength' => '100',
                                          'class' => 'form-control',
                                        ]) 
                                    ?>
                                </div>
                                <?php
                          endif;
                        endforeach;
                    endif
                  ?>
                  <!-- =====================Content==================== -->
                  <?php
                  if(!empty($langsetlang)) :
                          foreach ($langsetlang as $lang =>$valuelang):
                              if(!empty($valuelang)) :
                                ?>
                                <!-- <input type="text" name="a"> -->
                                <div class="form-group col-sm-12 lang_hidden lang_<?=$valuelang['type']?> <?=$valuelang['type']=='vi_VN'?'active':''?>">
                                  <!-- <label for="inputnametitle" class="col-sm-2 control-label">Tiêu đề (<?=$valuelang["name"]?>):</label> -->
                                    <?= $this->Form->control('content_'.$valuelang["type"], 
                                        [
                                          'label' => 'Nội dung ('.$valuelang["name"].')', 
                                          'type' => 'textarea', 
                                          'maxlength' => '100',
                                          'class' => 'form-control',
                                          'id' => 'noidung_'.$valuelang["type"],
                                          'data-course-name' => '',
                                        ]) 
                                    ?>
                                </div>
                                <?php
                          endif;
                        endforeach;
                    endif
                  ?>
              </div>
              <div class="row"> 
                   <div class="form-group col-sm-12">
                      <?= $this->Form->control('title', 
                                        [
                                          'label' => 'Title', 
                                          'type' => 'text', 
                                          'class' => 'form-control',
                                          'data-course-name' => '',
                                        ]) 
                    ?>
                  </div>
                  <div class="form-group col-sm-12">
                      <?= $this->Form->control('keywords', 
                                        [
                                          'label' => 'Từ Khóa', 
                                          'type' => 'text',
                                          'class' => 'form-control',
                                          'data-course-name' => '',
                                        ]) 
                    ?>
                  </div>
                  <div class="form-group col-sm-12">
                      <?= $this->Form->control('description', 
                                        [
                                          'label' => 'description', 
                                          'type' => 'textarea',
                                          'class' => 'form-control',
                                          'data-course-name' => '',
                                        ]) 
                    ?>
                  </div>    
              </div>
                  <div class="form-group">    
                      <button type="submit" class="btn btn-primary" style="float: right;">Thêm mới</button>
                  </div>
            <?=
              $this->Form->end();
             ?>
          </div>
      </div>
    </div>
  </div>
<?php        
  if(!empty($langsetlang)) :
      foreach ($langsetlang as $lang =>$valuelang):
          if(!empty($valuelang)) :
            ?>
            <!-- <input type="text" name="a"> -->
            <script type="text/javascript">
              CKEDITOR.replace( 'noidung_<?=$valuelang["type"]?>' );
            </script>
            <?php
      endif;
    endforeach;
  endif
?>
<script type="text/javascript">   
  $(document).ready(function() {
    $('.chonngonngu li').click(function(event) {
      var lang = $(this).attr('data-lang');
      $('.chonngonngu li').removeClass('active');
      $(this).addClass('active');
      $('.lang_hidden').removeClass('active');
      $('.lang_'+lang).addClass('active');
      return false;
    });
  });
</script>
