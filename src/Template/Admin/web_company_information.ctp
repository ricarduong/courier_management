<div class="row ">
    <div class="page-header-top">
      <div class="col-lg-12 col-md-12">
          <h4>Thông tin Web</h4>
          <hr>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
    <div class="panel panel-default">
            <div class="title chonngonngu">
                <ul class="nav nav-tabs nav-justified">
                <?php 
                   if(!empty($langsetlang)) :
                        foreach ($langsetlang as $lang =>$valuelang):
                            if(!empty($valuelang)) :
                ?>
                                <li class="<?= $valuelang['type']=='vi_VN'?'active':'' ?>" data-lang="<?=$valuelang['type']?>">
                                <a href="" title="Chọn <?=$valuelang['name']?> ">
                                <?=$valuelang['name']?>
                                </a>
                              </li>
                <?php
                        endif;
                      endforeach;
                  endif;
                ?>
                </ul>
            </div>
        <div class="panel-body">
            <?php 
                if(!empty($data_setting)) :
            ?>
            <?=
                $this->Form->create();
            ?>
            <div class="row"> 
                <?php 
                if(!empty($langsetlang)) :
                    foreach ($langsetlang as $lang =>$valuelang):
                        if(!empty($valuelang)) :
                ?>
                        <div class="form-group col-sm-6 lang_hidden lang_<?=$valuelang['type']?> <?=$valuelang['type']=='vi_VN'?'active':''?>">
                            <?= $this->Form->control('name_'.$valuelang["type"], 
                                [
                                    'label' => 'Tên Công Ty ('.$valuelang["name"].')', 
                                    'type' => 'text', 
                                    'value'=> $data_setting['name_'.$valuelang["type"]],             
                                    'maxlength' => '100',
                                    'class' => 'form-control',
                                    'data-course-name' => '',
                                ]) 
                            ?>
                        </div>
                <?php
                        endif;
                      endforeach;
                  endif
                ?>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('email', 
                                      [
                                        'label' => 'email', 
                                        'type' => 'email', 
                                        'value'=> $data_setting['email'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('hotline', 
                                      [
                                        'label' => 'Hotline', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['hotline'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('phone', 
                                      [
                                        'label' => 'Điện Thoại', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['phone'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <?php 
                if(!empty($langsetlang)) :
                    foreach ($langsetlang as $lang =>$valuelang):
                        if(!empty($valuelang)) :
                ?>
                           <!-- <input type="text" name="a"> -->
                           <div class="form-group col-sm-6 lang_hidden lang_<?=$valuelang['type']?> <?=$valuelang['type']=='vi_VN'?'active':''?>">
                            <!-- <label for="inputnametitle" class="col-sm-2 control-label">Tiêu đề (<?=$valuelang["name"]?>):</label> -->
                                <?= $this->Form->control('address_'.$valuelang["type"], 
                                  [
                                    'label' => 'Địa chỉ ('.$valuelang["name"].')', 
                                    'type' => 'text', 
                                    'value'=> $data_setting['address_'.$valuelang["type"]],             
                                    'maxlength' => '100',
                                    'class' => 'form-control',
                                    'data-course-name' => '',
                                  ]) 
                                ?>      
                          </div>
                <?php
                        endif;
                    endforeach;
                endif
                ?>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('website', 
                                      [
                                        'label' => 'Website', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['website'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('bank', 
                                      [
                                        'label' => 'Bank', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['bank'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('account_num', 
                                      [
                                        'label' => 'Số tài khoản', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['account_num'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('coordinate', 
                                      [
                                        'label' => 'Tọa độ bản đồ', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['coordinate'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('facebook', 
                                      [
                                        'label' => 'Fanfage Facebook', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['facebook'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('facebook_link', 
                                      [
                                        'label' => ' Facebook link', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['facebook_link'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('youtube_link', 
                                      [
                                        'label' => ' Youtube link', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['youtube_link'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('skype_link', 
                                      [
                                        'label' => 'Skype link', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['skype_link'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $this->Form->control('google_link', 
                                      [
                                        'label' => 'Google link', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['google_link'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
            </div>
            <div class="row"> 
                 <div class="form-group col-sm-12">
                    <?= $this->Form->control('title', 
                                      [
                                        'label' => 'Title', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['title'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-12">
                    <?= $this->Form->control('keywords', 
                                      [
                                        'label' => 'Từ Khóa', 
                                        'type' => 'text', 
                                        'value'=> $data_setting['keywords'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-12">
                    <?= $this->Form->control('description', 
                                      [
                                        'label' => 'description', 
                                        'type' => 'textarea', 
                                        'value'=> $data_setting['description'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-12">
                    <?= $this->Form->control('analytics', 
                                      [
                                        'label' => 'analytics', 
                                        'type' => 'textarea', 
                                        'value'=> $data_setting['analytics'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
                <div class="form-group col-sm-12">
                    <?= $this->Form->control('vchat', 
                                      [
                                        'label' => 'V Chat', 
                                        'type' => 'textarea', 
                                        'value'=> $data_setting['vchat'],
                                        'class' => 'form-control',
                                        'data-course-name' => '',
                                      ]) 
                    ?>
                </div>
            </div>
            <div class="form-group">    
                <button type="submit" class="btn btn-primary" style="float: right;">Cập nhật</button>
            </div>
        <?=
            $this->Form->end();
        ?>
        <?php
             endif;
        ?>
    </div>
</div>
    </div>
<script type="text/javascript">   
  $(document).ready(function() {
    $('.chonngonngu li').click(function(event) {
      var lang = $(this).attr('data-lang');
      $('.chonngonngu li').removeClass('active');
      $(this).addClass('active');
      $('.lang_hidden').removeClass('active');
      $('.lang_'+lang).addClass('active');
      return false;
    });
  });
</script>
