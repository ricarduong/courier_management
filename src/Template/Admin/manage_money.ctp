<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h3>Quản lý tiền tệ</h3>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default fund-and-account">
            <div class="panel-heading">
                <b>Tiền tệ</b>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('addMoney',
                         [  
                            'class' => 'form-vertical',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->control('type_code', [
                                                'type' => 'text',
                                                'label' => 'Mã tiền tệ',
                                                'class' => 'form-control',
                                                'required' => true,
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->control('money_name', [
                                                'label' => 'Tên tiền tệ',
                                                'class' => 'form-control',
                                                'required' => true,
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->control('note', [
                                                'type' => 'textarea',
                                                'label' => 'Ghi chú',
                                                'class' => 'form-control',
                                                'rows' => 2
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Thêm', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-xs hidden-sm btn-search-payment-slip pull-right'
                            ]);
                        ?>
                        <?=
                            $this->Form->button('Thêm', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-lg hidden-md pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                DANH SÁCH TIỀN TỆ
            </div>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all">
                    <thead>
                        <tr>
                            <th>Mã tiền tệ</th>
                            <th>Tên tiền tệ</th>
                            <th>Ghi chú</th>
                        </tr>
                    </thead>
                    <?php
                        if (!empty($data_money)):
                            foreach ($data_money as $key => $value):
                    ?>
                        <tr>
                            <td><?= $value['type_code'] ?></td>
                            <td><?= $value['money_name'] ?></td>
                            <td><?= $value['note'] ?></td>
                        </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>