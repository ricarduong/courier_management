<table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
   class="table table-bordered table-hover row-border bm-table-list-all" table-list-checkbox>
    <thead>
        <tr>
            <th>Quỹ/Tài khoản</th>
            <th>Ngày thu</th>
            <th>Khoản thu</th>
            <th>Số tiền</th>
            <th>Số phiếu</th>
            <th>Họ và Tên</th>
            <th>Lý do</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if (!empty($payment_pdf)):
                foreach ($payment_pdf as $key => $value):
        ?>
            <tr>
                <td><?= $value['fund']['fund_name'] ?></td>
                <td><?= date('d/m/Y', strtotime($value['date'])) ?></td>
                <td><?= $value['income_and_expenditure']['name'] ?></td>
                <td><?= number_format($value['money']) ?></td>
                <td><?= $value['payment_slip_code'] ?></td>
                <td><?= $value['operator']['operator_name'] ?></td>
                <td><?= $value['reason'] ?></td>
            </tr>
        <?php
                endforeach;
            endif;
        ?>
    </tbody>
</table>