<?php
use Cake\Core\Configure;

$select_time = Configure::read('SELECT_TIME');
$operator = Configure::read('OPERATOR');
foreach ($data as $key => $value) {
    $data = $value;
}
$data['no_bill'] = substr($data['payment_slip_code'], 8);
?>
<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h3>Cập nhật phiếu chi</h3>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>Chi tiết phiếu chi</b></h4>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('data',
                         [
                            'class' => 'form-horizontal',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 col-xs-12 control-label">Ngày chi</label>
                                    <div class="input-group col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('date', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $date = empty($data['date']) ? '' : date('d/m/Y', strtotime($data['date'])),
                                                    'readonly' => true,
                                                    'input-date-picker',
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" date-picker><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group" number-input>
                                    <label class="col-sm-3 col-xs-12 control-label">Số tiền</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('money', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $money = empty($data['money']) ? '' : number_format($data['money']),
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">Số phiếu</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('no_bill', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-no-bill',
                                                    'value' => $no_bill = empty($data['no_bill']) ? '' : $data['no_bill'],
                                                    'readonly' => true
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">Mã phiếu</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('payment_slip_code', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-payment-slip-code',
                                                    'value' => $payment_slip_code = empty($data['payment_slip_code']) ? '' : $data['payment_slip_code'],
                                                    'readonly' => true
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">Họ & Tên</label>
                                    <div class="input-group col-sm-9" data-input-name-ope>
                                        <?=
                                            $this->Form->control('operator.id_operator',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control hidden',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'id-ope' => 'id',
                                                    'readonly' => true,
                                                    'id-edit' => $id_operator = empty($data['operator']['id_operator']) ? '' : $data['operator']['id_operator']
                                                ]
                                            );
                                        ?>
                                        <?=
                                            $this->Form->control('operator.operator_name',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'name-ope' => 'name',
                                                    'data-ope-name',
                                                    'readonly' => true,
                                                    'name-edit' => $name_operator = empty($data['operator']['operator_name']) ? '' : $data['operator']['operator_name']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle ="modal" data-target="#add-name-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">Khoản chi</label>
                                    <div class="input-group col-sm-9" data-input-expenses>
                                       <?=
                                            $this->Form->select('id_income_and_expenditure',
                                                $expenses,
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn khoản chi',
                                                    'data-json-expenses' => json_encode($expenses),
                                                    'value' => $id_expense = empty($data['id_income_and_expenditure']) ? 0 : $data['id_income_and_expenditure']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle="modal" data-target="#add-expenses-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Kèm theo</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('vouchers', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Chứng từ gốc',
                                                    'value' => $vouchers = empty($data['vouchers']) ? '' : $data['vouchers']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Quỹ/Tài khoản</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->select('fund.id_fund',
                                                $data_fund,
                                                [
                                                    'label' => true,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn quỹ / tài khoản',
                                                    'value' => $id_fund = empty($data['fund']['id_fund']) ? 0 : $data['fund']['id_fund'] 
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ</label>
                                    <div class="col-sm-9" data-input-address-ope>
                                        <?=
                                            $this->Form->control('operator.address', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-address',
                                                    'readonly' => true,
                                                    'address-edit' => $address = empty($data['operator']['address']) ? '' : $data['operator']['address']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Lý do</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('reason', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $reason = empty($data['reason']) ? '' : $data['reason']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('note', [
                                                    'type' => 'textarea',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'rows' => 3,
                                                    'value' => $note = empty($data['note']) ? '' : $data['note']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Xác nhận', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                
                <?= $this->Form->end(); ?>
            </div>
             <?= $this->element('Modal/select_name_modal', ['data_operator' => $data_operator]); ?>
            <?=

                $this->element('Modal/add_operator_modal',['data_type' => $data_type]);
            ?>
            <?= $this->element('Modal/add_expenses_modal'); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('[input-date-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('[date-picker]').click(function(){
            $('[input-date-picker]').focus();
        });
    })
</script>