<?php
use Cake\Core\Configure;

$select_time = Configure::read('SELECT_TIME');
$remark = Configure::read('REMARK');
$now = date('d/m/Y');
// $year = date('y');
// $mon = date('m');
// $payment_slip_code = 'PT/'.$year.$mon.'/';
// $payment_slip_end = $last_receipt;
//     $code_mon = substr($payment_slip_end['receipt_code'], 3, 2);
//     $code_year = substr($payment_slip_end['receipt_code'], 5, 2);
//     if ($year == $code_year && $mon == $code_mon) {
//         $no_bill = substr($payment_slip_end['receipt_code'], 8) + 1;
//     }else{
//         $no_bill = 1;
//     }
?>
<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h3>Quản lý công nợ</h3>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>Chi tiết công nợ</b></h4>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('data',
                         [  
                            'class' => 'form-horizontal form-receipt',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 col-xs-12 control-label">Ngày</label>
                                    <div class="input-group col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('date', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $date = empty($data['date']) ? $now : $data['date'],
                                                    'readonly' => true,
                                                    'input-date-picker',
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" date-picker><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="form-group" number-input>
                                    <label class="col-sm-5 col-xs-12 control-label">PGS</label>
                                    <div class="col-sm-7 col-xs-12">
                                        <?=
                                            $this->Form->control('PGS', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'vnd-money',
                                                    'value' => $money = empty($data['VND']) ? '' : $data['VND'],
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">WT</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('weight', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'usd-money',
                                                    'value' => $money = empty($data['USD']) ? '' : $data['USD'],
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">NOTIFY</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('notify', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control'
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">Country</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('country', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">CONSIGNEE</label>
                                    <div class="input-group col-sm-9" data-input-name-ope>
                                        <?=
                                            $this->Form->control('operator.id_operator',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control hidden',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'id-ope' => 'id',
                                                    'readonly' => true,
                                                    'value' => $id_operator = empty($data['operator']['id_operator']) ? '' : $data['operator']['id_operator']
                                                ]
                                            );
                                        ?>
                                        <?=
                                            $this->Form->control('operator.name_operator',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'name-ope' => 'name',  
                                                    'readonly' => true,
                                                    'value' => $name_operator = empty($data['operator']['name_operator']) ? '' : $data['operator']['name_operator']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle ="modal" data-target="#add-name-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">WAREHOUSE</label>
                                    <div class="input-group col-sm-9">
                                       <?=
                                            $this->Form->control('warehouse',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-json-expenses' => $incomes = empty($incomes) ? '' : json_encode($incomes),
                                                    'value' => $id_expense = empty($data['warehouse']) ? 0 : $data['warehouse']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-6 col-xs-12 control-label">PP/CC</label>
                                    <div class="col-sm-6 col-xs-12">
                                        <?=
                                            $this->Form->select('pp_cc',
                                                $remark,
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => true,
                                                    'value' => $id_expense = empty($data['remark']) ? 0 : $data['remark']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-4 col-xs-12 control-label">CONSOL</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <?=
                                            $this->Form->control('consol', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $no_bill = empty($data['PCS']) ? '' : $data['PCS']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">SHPR NAME</label>
                                    <div class="input-group col-sm-9" data-input-shipper>
                                       <?=
                                            $this->Form->select('shipper.id_shipper',
                                                $shippers = empty($shippers) ? [1,2,3] : $shippers,
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn Shipper',
                                                    'data-json-shipper' => $shippers = empty($shippers) ? 0 : json_encode($shippers),
                                                    'value' => $id_expense = empty($data['shipper']['id_shipper']) ? 0 : $data['shipper']['id_shipper']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle="modal" data-target="#add-shipper-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">HAWB NO</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('hawb_no', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $vouchers = empty($data['vouchers']) ? '' : $data['vouchers']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">VALUE</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('value',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $id_fund = empty($data['fund']['id_fund']) ? 0 : $data['fund']['id_fund'] 
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Notify address</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('notify_address', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Customer CD</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('customer_code', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $reason = empty($data['reason']) ? '' : $data['reason']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('desription', [
                                                    'type' => 'textarea',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'rows' => 3,
                                                    'value' => $note = empty($data['note']) ? '' : $data['note']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-6 col-xs-12 control-label">HS CODE</label>
                                    <div class="col-sm-6 col-xs-12">
                                        <?=
                                            $this->Form->control('hs_code', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $no_bill = empty($data['KGS']) ? '' : $data['KGS']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-6 col-xs-12 control-label">USE TYPE</label>
                                    <div class="col-sm-6 col-xs-12">
                                        <?=
                                            $this->Form->control('use_type', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $no_bill = empty($data['KGS']) ? '' : $data['KGS']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Thêm', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>DANH SÁCH CÔNG NỢ</b></h4>
            </div>
            <div class="panel-heading">
                <?= $this->Form->create('addEmployee',
                        [   'url' => [
                                'controller' => 'admin',
                                'action' => 'searchMortgage'
                                ],
                            'class' => 'form-vertical',
                        ]
                    ); ?>
                <div class="row">
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Từ ngày</label>
                            <div class="input-group">
                                <?=
                                    $this->Form->control('date_from', [
                                            'label' => false,
                                            'class' => 'form-control',
                                            'value' => $date = empty($data_search['date_from']) ? '' : $data_search['date_from'],
                                            'readonly' => true,
                                            'input-date-from-picker',
                                        ]
                                    );
                                ?>
                                <span class="input-group-addon" date-from-picker><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Đến ngày</label>
                            <div class="input-group">
                                <?=
                                    $this->Form->control('date_to', [
                                            'label' => false,
                                            'class' => 'form-control',
                                            'value' => $date = empty($data_search['date_to']) ? '' : $data_search['date_to'],
                                            'readonly' => true,
                                            'input-date-to-picker',
                                        ]
                                    );
                                ?>
                                <span class="input-group-addon" date-to-picker><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>                    
                    <div class="col-lg-1 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Search', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-xs hidden-sm btn-search-payment-slip'
                            ]);
                        ?>
                        <?=
                            $this->Form->button('Search', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-lg hidden-md pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                    <?= $this->Form->end(); ?>
            </div>
            <?= $this->element('Modal/select_name_modal', ['data_operator' => $data_operator]); ?>
            <?=
                $this->element('Modal/add_operator_modal',['data_type' => $data_type]);
            ?>
            <?= $this->element('Modal/add_expenses_modal', ['IE' => 'I']); ?>
            <?= $this->element('Modal/add_shipper_modal'); ?>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all" table-list-checkbox list-check-box table-before-search>
                    <thead>
                        <tr>
                            <th>HAWB NO</th>
                            <th>PGS</th>
                            <th>WT</th>
                            <th>VALUE</th>
                            <th>DESCRIPTION</th>
                            <th>SHIPPER</th>
                            <th>CONSIGNEE</th>
                            <th>C ADDRESS</th>
                            <th>PP/CC</th>
                            <th>CNEE Phone</th>
                            <th>Sửa</th>
                            <th>Xóa</th>
                            <th>In</th>
                        </tr>
                        <?php 
                            // echo $this->Form->create('addEmployee',
                            //     [   
                            //         'class' => 'form-horizontal',
                            //         'id' => 'form-data-search'
                            //     ]
                            // );
                        ?>
                        <!-- <tr tr-search-element>
                            <td></td>
                            <td>
                                <?=
                                    $this->Form->select('fund_account',
                                        $data_fund = empty($data_fund) ? [0,1,2] : $data_fund,
                                        [
                                            'class' => 'form-control input-search',
                                            'empty' => 'Chọn Quỹ/Tài khoản'
                                        ]
                                    );
                                ?>
                            </td>
                            <td>
                                <?=
                                    $this->Form->control('date', [
                                            'label' => false,
                                            'class' => 'form-control input-search',
                                        ]
                                    );
                                ?>
                            </td>
                            <td><?=
                                $this->Form->control('income_name', [
                                        'label' => false,
                                        'class' => 'form-control input-search',
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('VND', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('USD', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('receipt_code', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('operator_name', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('reason', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('HB_NO', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('remark', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr> -->
                        <?php //$this->Form->end(); ?>
                    </thead>
                    <tbody>
                    <?php
                        if (!empty($data_mortgage)):
                            foreach ($data_mortgage as $key => $value):
                    ?>
                        <tr>
                            <!-- <td align="center" style="vertical-align: middle;"><?php
                                // $this->Form->checkbox('check_all', [
                                //         // 'value' => $value['collected'],
                                //         'data-checked'
                                //     ]
                                // );
                            ?></td> -->
                            <td><?= $value['hawb_no'] ?></td>
                            <td><?= $value['PGS'] ?></td>
                            <td><?= $value['weight'] ?></td>
                            <td><?= $value['value'] ?></td>
                            <td><?= $value['description'] ?></td>
                            <td><?= $value['shipper']['shipper_name'] ?></td>
                            <td><?= $value['operator']['operator_name'] ?></td>
                            <td><?= $value['operator']['address'] ?></td>
                            <td><?= $value['pp_cc'] ?></td>
                            <td><?= $value['operator']['phone'] ?></td>
                            <td align="center">
                                <?= $this->Html->link('<i class="fa fa-edit"></i>',
                                        [
                                            'controller' => 'admin',
                                            'action' => 'receiptEdit',
                                            $value['id_receipt']
                                        ],
                                        [
                                            'escape' => false
                                        ]
                                    );
                                ?>
                            </td>
                            <td align="center"><a href="deleteReceipt/<?= $value['id'] ?>" onclick="return confirm('Bạn đã chắc chắn xóa?');"><i class="fa fa-trash"></i></a></td>
                            <td align="center">
                                <?= $this->Html->link('<i class="fa fa-print fa-fw"></i>',
                                        [
                                            'controller' => 'admin',
                                            'action' => 'receiptPdf',
                                            $value['id_receipt']
                                        ],
                                        [
                                            'class'=>'pdf_report',
                                            'target'=>'_blank',
                                            'escape' => false,
                                            'link-print'
                                        ]
                                    );
                                ?></td>
                        </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </tbody>
                </table>
                <div class="col-lg-3 col-md-3 col-sm-6" data-vat>
                    <div class="row">
                        <label class="control-label">VAT</label>
                        <?=
                            $this->Form->checkbox('check_vat', [
                                    'value' => '0',
                                    'class' => 'control-label',
                                    'style' => 'margin-left: 10px;'
                                ]
                            );
                        ?>
                    </div>
                </div>
                <?php if($this->Paginator->total() > 1) : ?>
                    <div class="paginator col-lg-12">
                        <ul class="pagination pull-right cms-ul-navigation">
                            <?= $this->Paginator->first('<< ' . __('First')) ?>
                            <?= $this->Paginator->prev('< ' . __('Previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('Next') . ' >') ?>
                            <?= $this->Paginator->last(__('Last') . ' >>') ?>
                        </ul>
                        <p class="cms-margin-top-5"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                <?php endif; ?>
                <?=
                    $this->Html->link('Export', 
                        [
                            'controller' => 'admin',
                            'action' => 'receiptPdf',
                            '?' => $data_search = empty($data_search) ? '' : $data_search
                        ],
                        [
                            'class'=>'pdf_report btn btn-primary pull-right',
                            'target'=>'_blank',
                            'escape' => false,
                            'btn-printer'
                        ]
                    );
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('[input-date-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('[date-picker]').click(function(){
            $('[input-date-picker]').focus();
        });

        $('[input-date-from-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        
        $('[date-from-picker]').click(function(){
            $('[input-date-from-picker]').focus();
        });

        $('[input-date-to-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('[date-to-picker]').click(function(){
            $('[input-date-to-picker]').focus();
        });
    })
       
    
</script>