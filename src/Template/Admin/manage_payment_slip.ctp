<?php
use Cake\Core\Configure;

$select_time = Configure::read('SELECT_TIME');
$operator = Configure::read('OPERATOR');
$year = date('y');
$mon = date('m');
$payment_slip_code = 'PC/'.$year.$mon.'/';
$payment_slip_end = $last_payment;
    $code_mon = substr($payment_slip_end['payment_slip_code'], 3, 2);
    $code_year = substr($payment_slip_end['payment_slip_code'], 5, 2);
    if ($year == $code_year && $mon == $code_mon) {
        $no_bill = substr($payment_slip_end['payment_slip_code'], 8) + 1;
    }else{
        $no_bill = 1;
    }
?>
<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h3>Quản lý phiếu chi</h3>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>Chi tiết phiếu chi</b></h4>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('data',
                         [  
                            'class' => 'form-horizontal',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 col-xs-12 control-label">Ngày chi</label>
                                    <div class="input-group col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('date', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $date = empty($data['date']) ? '' : $data['date'],
                                                    'readonly' => true,
                                                    'input-date-picker',
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" date-picker><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group" number-input>
                                    <label class="col-sm-3 col-xs-12 control-label">Số tiền</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('money', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $money = empty($data['money']) ? '' : $data['money'],
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">Số phiếu</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('no_bill', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-no-bill',
                                                    'value' => $no_bill = empty($data['no_bill']) ? $no_bill : $data['no_bill']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">Mã phiếu</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('payment_slip_code', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-payment-slip-code',
                                                    'readonly' => true,
                                                    'value' => $payment_slip_code = empty($data['payment_slip_code']) ? $payment_slip_code : $data['payment_slip_code'],
                                                    'data-confirm' => 'E'
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">Họ & Tên</label>
                                    <div class="input-group col-sm-9" data-input-name-ope>
                                        <?=
                                            $this->Form->control('operator.id_operator',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control hidden',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'id-ope' => 'id',
                                                    'readonly' => true,
                                                    'value' => $id_operator = empty($data['operator']['id_operator']) ? '' : $data['operator']['id_operator']
                                                ]
                                            );
                                        ?>
                                        <?=
                                            $this->Form->control('operator.name_operator',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'name-ope' => 'name',  
                                                    'readonly' => true,
                                                    'value' => $name_operator = empty($data['operator']['name_operator']) ? '' : $data['operator']['name_operator']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle ="modal" data-target="#add-name-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">Khoản chi</label>
                                    <div class="input-group col-sm-9" data-input-expenses>
                                       <?=
                                            $this->Form->select('id_income_and_expenditure',
                                                $expenses,
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn khoản chi',
                                                    'data-json-expenses' => json_encode($expenses),
                                                    'value' => $id_expense = empty($data['id_income_and_expenditure']) ? 0 : $data['id_income_and_expenditure']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle="modal" data-target="#add-expenses-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Kèm theo</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('vouchers', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Chứng từ gốc',
                                                    'value' => $vouchers = empty($data['vouchers']) ? '' : $data['vouchers']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Quỹ/Tài khoản</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->select('fund.id_fund',
                                                $data_fund,
                                                [
                                                    'label' => true,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn quỹ / tài khoản',
                                                    'value' => $id_fund = empty($data['fund']['id_fund']) ? 0 : $data['fund']['id_fund'] 
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ</label>
                                    <div class="col-sm-9" data-input-address-ope>
                                        <?=
                                            $this->Form->control('operator.address', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-address',
                                                    'readonly' => true,
                                                    'value' => $address = empty($data['operator']['address']) ? 0 : $data['operator']['address']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Lý do</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('reason', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $reason = empty($data['reason']) ? '' : $data['reason']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('note', [
                                                    'type' => 'textarea',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'rows' => 3,
                                                    'value' => $note = empty($data['note']) ? '' : $data['note']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Thêm', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>DANH SÁCH PHIẾU CHI</b></h4>
            </div>
            <div class="panel-heading">
                <?= $this->Form->create('addEmployee',
                        [   'url' => [
                                'controller' => 'admin',
                                'action' => 'searchPaymentSlip'
                                ],
                            'class' => 'form-vertical',
                        ]
                    ); ?>
                <div class="row">
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Từ ngày</label>
                            <div class="input-group">
                                <?=
                                    $this->Form->control('date_from', [
                                            'label' => false,
                                            'class' => 'form-control',
                                            'value' => $date = empty($data_search['date_from']) ? '' : $data_search['date_from'],
                                            'readonly' => true,
                                            'input-date-from-picker',
                                        ]
                                    );
                                ?>
                                <span class="input-group-addon" date-from-picker><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Đến ngày</label>
                            <div class="input-group">
                                <?=
                                    $this->Form->control('date_to', [
                                            'label' => false,
                                            'class' => 'form-control',
                                            'value' => $date = empty($data_search['date_to']) ? '' : $data_search['date_to'],
                                            'readonly' => true,
                                            'input-date-to-picker',
                                        ]
                                    );
                                ?>
                                <span class="input-group-addon" date-to-picker><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label for="email">Quỹ/Tài khoản</label>
                            <?=
                                $this->Form->select('fund_account', 
                                    $data_fund,
                                    [
                                        'class' => 'form-control',
                                        'empty' => 'Chọn Quỹ/Tài khoản',
                                        'value' => $id_fund = empty($data_search['fund_account']) ? '' : $data_search['fund_account']
                                    ]
                                );
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="form-group">
                            <label for="email">Nhập thông tin</label>
                            <?=
                                $this->Form->control('info', [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'value' => $info = empty($data_search['info']) ? '' : $data_search['info']
                                    ]
                                );
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Search', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-xs hidden-sm btn-search-payment-slip pull-right'
                            ]);
                        ?>
                        <?=
                            $this->Form->button('Search', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary col-lg-2 hidden-lg hidden-md pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                    <?= $this->Form->end(); ?>
            </div>
            <?= $this->element('Modal/select_name_modal', ['data_operator' => $data_operator]); ?>
            <?=

                $this->element('Modal/add_operator_modal',['data_type' => $data_type]);
            ?>
            <?= $this->element('Modal/add_expenses_modal',['IE' => 'E']); ?>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all" table-list-checkbox>
                    <thead>
                        <tr>
                            <th><?=
                                $this->Form->checkbox('check_all', [
                                        'value' => '10'
                                    ]
                                );
                            ?></th>
                            <th>Quỹ/Tài khoản</th>
                            <th>Ngày chi</th>
                            <th>Khoản chi</th>
                            <th>Số tiền</th>
                            <th>Số phiếu</th>
                            <th>Họ và Tên</th>
                            <th>Địa chỉ</th>
                            <th>Lý do</th>
                            <th>Người lập</th>
                            <th>Sửa</th>
                            <th>Xóa</th>
                            <th>In</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if (!empty($data_payment_slips)):
                            foreach ($data_payment_slips as $key => $value):
                    ?>
                        <tr>
                            <td><?=
                                $this->Form->checkbox('check_all', [
                                        'value' => $value['id_payment_slip']
                                    ]
                                );
                            ?></td>
                            <td><?= $value['fund']['fund_name'] ?></td>
                            <td><?= date('d/m/Y', strtotime($value['date'])) ?></td>
                            <td><?= $value['income_and_expenditure']['name'] ?></td>
                            <td><?= number_format($value['money']) ?></td>
                            <td><?= $value['payment_slip_code'] ?></td>
                            <td><?= $value['operator']['operator_name'] ?></td>
                            <td><?= $value['address'] ?></td>
                            <td><?= $value['reason'] ?></td>
                            <td><?= $user['username'] ?></td>
                            <td align="center">
                                <?= $this->Html->link('<i class="fa fa-edit"></i>',
                                        [
                                            'controller' => 'admin',
                                            'action' => 'paymentEdit',
                                            $value['id_payment_slip']
                                        ],
                                        [
                                            'escape' => false
                                        ]
                                    );
                                ?>
                            </td>
                            <td align="center"><a href="deletePayment/<?= $value['id_payment_slip'] ?>" onclick="return confirm('Bạn đã chắc chắn xóa?');"><i class="fa fa-trash"></i></a></td>
                            <td align="center">
                                <?= $this->Html->link('<i class="fa fa-print fa-fw"></i>',
                                        [
                                            'controller' => 'admin',
                                            'action' => 'paymentSlipPdf',
                                            $value['id_payment_slip']
                                        ],
                                        [
                                            'class'=>'pdf_report',
                                            'target'=>'_blank',
                                            'escape' => false,
                                            'link-print'
                                        ]
                                    );
                                ?></td>
                        </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </tbody>
                </table>
                <?php if($this->Paginator->total() > 1) : ?>
                    <div class="paginator col-lg-12">
                        <ul class="pagination pull-right cms-ul-navigation">
                            <?= $this->Paginator->first('<< ' . __('First')) ?>
                            <?= $this->Paginator->prev('< ' . __('Previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('Next') . ' >') ?>
                            <?= $this->Paginator->last(__('Last') . ' >>') ?>
                        </ul>
                        <p class="cms-margin-top-5"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                <?php endif; ?>
                <?=
                    $this->Html->link('Export', 
                        [
                            'controller' => 'admin',
                            'action' => 'paymentSlipPdf',
                            '?' => $data_search
                        ],
                        [
                            'class'=>'pdf_report btn btn-primary pull-right',
                            'target'=>'_blank',
                            'escape' => false,
                            'link-print'
                        ]
                    );
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('[input-date-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('[date-picker]').click(function(){
            $('[input-date-picker]').focus();
        });

        $('[input-date-from-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        
        $('[date-from-picker]').click(function(){
            $('[input-date-from-picker]').focus();
        });

        $('[input-date-to-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('[date-to-picker]').click(function(){
            $('[input-date-to-picker]').focus();
        });
    })
       
    
</script>