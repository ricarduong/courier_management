<?php
use Cake\Core\Configure;

$select_time = Configure::read('SELECT_TIME');
$remark = Configure::read('REMARK');
$now = date('d/m/Y');
$year = date('y');
$mon = date('m');
$payment_slip_code = 'PT/'.$year.$mon.'/';
$payment_slip_end = $last_receipt;
    $code_mon = substr($payment_slip_end['receipt_code'], 3, 2);
    $code_year = substr($payment_slip_end['receipt_code'], 5, 2);
    if ($year == $code_year && $mon == $code_mon) {
        $no_bill = substr($payment_slip_end['receipt_code'], 8) + 1;
    }else{
        $no_bill = 1;
    }
?>
<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h3>Quản lý phiếu thu</h3>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>Chi tiết phiếu thu</b></h4>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('data',
                         [  
                            'class' => 'form-horizontal form-receipt',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 col-xs-12 control-label">Ngày thu</label>
                                    <div class="input-group col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('date', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $date = empty($data['date']) ? $now : $data['date'],
                                                    'readonly' => true,
                                                    'input-date-picker',
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" date-picker><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="form-group" number-input>
                                    <label class="col-sm-5 col-xs-12 control-label">Số tiền</label>
                                    <div class="col-sm-7 col-xs-12">
                                        <?=
                                            $this->Form->control('VND', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'vnd-money',
                                                    'value' => $money = empty($data['VND']) ? '' : $data['VND'],
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">USD</label>
                                    <div class="col-sm-9 col-xs-12">
                                        <?=
                                            $this->Form->control('USD', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'usd-money',
                                                    'value' => $money = empty($data['USD']) ? '' : $data['USD'],
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-5 col-xs-12 control-label">Số phiếu</label>
                                    <div class="col-sm-7 col-xs-12">
                                        <?=
                                            $this->Form->control('no_bill', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-no-bill',
                                                    'value' => $no_bill = empty($data['no_bill']) ? $no_bill : $data['no_bill']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <div class="input-group col-sm-12 col-xs-12">
                                        <?=
                                            $this->Form->control('change', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $exchange_rate = ($currency[0]['currency_name'] == 'USD') ? number_format($currency[0]['exchange_rate']) : 0,
                                                    'readonly' => true,
                                                    'change-money'
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle ="modal" data-target="#change-money-modal">Đổi</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 col-xs-12 control-label">Mã phiếu</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('receipt_code', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-payment-slip-code',
                                                    'readonly' => true,
                                                    'value' => $payment_slip_code = empty($data['receipt_code']) ? $payment_slip_code : $data['receipt_code'],
                                                    'data-confirm' => 'I'
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">Họ & Tên</label>
                                    <div class="input-group col-sm-9" data-input-name-ope>
                                        <?=
                                            $this->Form->control('operator.id_operator',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control hidden',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'id-ope' => 'id',
                                                    'readonly' => true,
                                                    'value' => $id_operator = empty($data['operator']['id_operator']) ? '' : $data['operator']['id_operator']
                                                ]
                                            );
                                        ?>
                                        <?=
                                            $this->Form->control('operator.name_operator',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#select-name-modal',
                                                    'name-ope' => 'name',  
                                                    'readonly' => true,
                                                    'value' => $name_operator = empty($data['operator']['name_operator']) ? '' : $data['operator']['name_operator']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle ="modal" data-target="#add-name-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">Khoản thu</label>
                                    <div class="input-group col-sm-9" data-input-expenses>
                                       <?=
                                            $this->Form->select('id_income_and_expenditure',
                                                $incomes,
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn khoản thu',
                                                    'data-json-expenses' => json_encode($incomes),
                                                    'value' => $id_expense = empty($data['id_income_and_expenditure']) ? 0 : $data['id_income_and_expenditure']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle="modal" data-target="#add-expenses-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-6 col-xs-12 control-label">REMARK</label>
                                    <div class="col-sm-6 col-xs-12">
                                        <?=
                                            $this->Form->select('remark',
                                                $remark,
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => true,
                                                    'value' => $id_expense = empty($data['remark']) ? 0 : $data['remark']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-4 col-xs-12 control-label">PCS</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <?=
                                            $this->Form->control('PCS', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $no_bill = empty($data['PCS']) ? '' : $data['PCS']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-3 control-label">SHPR NAME</label>
                                    <div class="input-group col-sm-9" data-input-shipper>
                                       <?=
                                            $this->Form->select('shipper.id_shipper',
                                                $shippers,
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn Shipper',
                                                    'data-json-shipper' => json_encode($shippers),
                                                    'value' => $id_expense = empty($data['shipper']['id_shipper']) ? 0 : $data['shipper']['id_shipper']
                                                ]
                                            );
                                        ?>
                                        <span class="input-group-addon" data-toggle="modal" data-target="#add-shipper-modal"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Kèm theo</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('vouchers', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Chứng từ gốc',
                                                    'value' => $vouchers = empty($data['vouchers']) ? '' : $data['vouchers']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Quỹ/Tài khoản</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->select('fund.id_fund',
                                                $data_fund,
                                                [
                                                    'label' => true,
                                                    'class' => 'form-control',
                                                    'empty' => 'Chọn quỹ / tài khoản',
                                                    'value' => $id_fund = empty($data['fund']['id_fund']) ? 0 : $data['fund']['id_fund'] 
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ</label>
                                    <div class="col-sm-9" data-input-address-ope>
                                        <?=
                                            $this->Form->control('operator.address', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'data-address',
                                                    'readonly' => true,
                                                    'value' => $address = empty($data['operator']['address']) ? 0 : $data['operator']['address']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Lý do</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('reason', [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $reason = empty($data['reason']) ? '' : $data['reason']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <?=
                                            $this->Form->control('note', [
                                                    'type' => 'textarea',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'rows' => 3,
                                                    'value' => $note = empty($data['note']) ? '' : $data['note']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-6 col-xs-12 control-label">KGS</label>
                                    <div class="col-sm-6 col-xs-12">
                                        <?=
                                            $this->Form->control('KGS', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $no_bill = empty($data['KGS']) ? '' : $data['KGS']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group manage-payment-slip">
                                    <label class="col-sm-4 control-label">H/B NO</label>
                                    <div class="col-sm-8">
                                        <?=
                                            $this->Form->control('HB_NO', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $note = empty($data['note']) ? '' : $data['note']
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-6 col-xs-12 control-label">Đã thu</label>
                                    <div class="col-sm-6 col-xs-12" collected-check>
                                        <?=
                                            $this->Form->checkbox('collected',
                                                [
                                                    'value' => $collected = empty($data['collected']) ? 0 : $data['collected'],
                                                    'data-collected',
                                                    'style' => 'margin-top: 10px;'
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Thêm', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>DANH SÁCH PHIẾU THU</b></h4>
            </div>
            <div class="panel-heading">
                <?= $this->Form->create('addEmployee',
                        [   'url' => [
                                'controller' => 'admin',
                                'action' => 'searchReceipt'
                                ],
                            'class' => 'form-vertical',
                        ]
                    ); ?>
                <div class="row">
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Từ ngày</label>
                            <div class="input-group">
                                <?=
                                    $this->Form->control('date_from', [
                                            'label' => false,
                                            'class' => 'form-control',
                                            'value' => $date = empty($data_search['date_from']) ? '' : $data_search['date_from'],
                                            'readonly' => true,
                                            'input-date-from-picker',
                                        ]
                                    );
                                ?>
                                <span class="input-group-addon" date-from-picker><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Đến ngày</label>
                            <div class="input-group">
                                <?=
                                    $this->Form->control('date_to', [
                                            'label' => false,
                                            'class' => 'form-control',
                                            'value' => $date = empty($data_search['date_to']) ? '' : $data_search['date_to'],
                                            'readonly' => true,
                                            'input-date-to-picker',
                                        ]
                                    );
                                ?>
                                <span class="input-group-addon" date-to-picker><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>                    
                    <div class="col-lg-1 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Search', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-xs hidden-sm btn-search-payment-slip'
                            ]);
                        ?>
                        <?=
                            $this->Form->button('Search', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-lg hidden-md pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                    <?= $this->Form->end(); ?>
            </div>
            <?= $this->element('Modal/select_name_modal', ['data_operator' => $data_operator]); ?>
            <?=
                $this->element('Modal/add_operator_modal',['data_type' => $data_type]);
            ?>
            <?= $this->element('Modal/add_expenses_modal', ['IE' => 'I']); ?>
            <?= $this->element('Modal/add_shipper_modal'); ?>
            <?= $this->element('Modal/change_money_modal', ['currency'=> $currency]); ?>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all" table-list-checkbox list-check-box table-before-search>
                    <thead>
                        <tr>
                            <th>Đã thu</th>
                            <th>Quỹ/Tài khoản</th>
                            <th>Ngày thu</th>
                            <th>Khoản thu</th>
                            <th>VND</th>
                            <th>USD</th>
                            <th>Số phiếu</th>
                            <th>Họ và Tên</th>
                            <th>Lý do</th>
                            <th>H/B NO</th>
                            <th>Remark</th>
                            <th>Sửa</th>
                            <th>Xóa</th>
                            <th>In</th>
                        </tr>
                        <?php 
                            echo $this->Form->create('addEmployee',
                                [   
                                    'class' => 'form-horizontal',
                                    'id' => 'form-data-search'
                                ]
                            );
                        ?>
                        <tr tr-search-element>
                            <td></td>
                            <td>
                                <?=
                                    $this->Form->select('fund_account',
                                        $data_fund,
                                        [
                                            'class' => 'form-control input-search',
                                            'empty' => 'Chọn Quỹ/Tài khoản'
                                        ]
                                    );
                                ?>
                            </td>
                            <td>
                                <?=
                                    $this->Form->control('date', [
                                            'label' => false,
                                            'class' => 'form-control input-search',
                                        ]
                                    );
                                ?>
                            </td>
                            <td>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu" style="padding-left: 10px;">
                                    <?php
                                    if(!empty($incomes)):
                                        foreach ($incomes as $key => $value) :
                                    ?>
                                  <li><?= $this->Form->checkbox('collected',
                                        [
                                            'value' => $key,
                                            'data-collected',
                                        ]
                                    );?> <?= $value ?></li>
                                    <?php 
                                        endforeach;
                                    endif;
                                    ?>
                                        <?=
                                            $this->Form->button('OK', [
                                                'type' => 'button',
                                                'class' => 'btn btn-primary',
                                                'btn-search-expense'
                                            ]);
                                        ?>
                                </ul>
                              </div>
                            </div>
                                <?php
                                // $this->Form->control('income_name', [
                                //         'label' => false,
                                //         'class' => 'form-control input-search',
                                //     ]
                                // );
                            ?></td>
                            <td><?=
                                $this->Form->control('VND', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('USD', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('receipt_code', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu" style="padding-left: 10px; width: 200px;">
                                        <?php
                                        if(!empty($data_operator)):
                                            foreach ($data_operator as $key => $value) :
                                        ?>
                                      <li><?= $this->Form->checkbox('collected',
                                            [
                                                'value' => $value['id_operator'],
                                                'data-collected',
                                            ]
                                        );?> <?= $value['operator_name'] ?></li>
                                        <?php 
                                            endforeach;
                                        endif;
                                        ?>
                                        <div>
                                            <?=
                                                $this->Form->button('OK', [
                                                    'type' => 'button',
                                                    'class' => 'btn btn-primary',
                                                    'btn-search-operator'
                                                ]);
                                            ?>
                                        </div> 
                                    </ul>
                                </div>
                            </td>
                            <td><?=
                                $this->Form->control('reason', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('HB_NO', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td><?=
                                $this->Form->control('remark', [
                                        'label' => false,
                                        'class' => 'form-control input-search'
                                    ]
                                );
                            ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?= $this->Form->end(); ?>
                    </thead>
                    <tbody>
                    <?php
                        if (!empty($data_receipt)):
                            foreach ($data_receipt as $key => $value):
                    ?>
                        <tr>
                            <td align="center" style="vertical-align: middle;"><?=
                                $this->Form->checkbox('check_all', [
                                        'value' => $value['collected'],
                                        'data-checked'
                                    ]
                                );
                            ?></td>
                            <td><?= $value['fund']['fund_name'] ?></td>
                            <td><?= date('d/m/Y', strtotime($value['date'])) ?></td>
                            <td><?= $value['income_and_expenditure']['name'] ?></td>
                            <td><?= number_format($value['VND']) ?></td>
                            <td><?= $value['USD'] ?></td>
                            <td><?= $value['receipt_code'] ?></td>
                            <td><?= $value['operator']['operator_name'] ?></td>
                            <td><?= $value['reason'] ?></td>
                            <td><?= $value['HB_NO'] ?></td>
                            <td><?= $value['remark'] ?></td>
                            <td align="center">
                                <?= $this->Html->link('<i class="fa fa-edit"></i>',
                                        [
                                            'controller' => 'admin',
                                            'action' => 'receiptEdit',
                                            $value['id_receipt']
                                        ],
                                        [
                                            'escape' => false
                                        ]
                                    );
                                ?>
                            </td>
                            <td align="center"><a href="deleteReceipt/<?= $value['id_receipt'] ?>" onclick="return confirm('Bạn đã chắc chắn xóa?');"><i class="fa fa-trash"></i></a></td>
                            <td align="center">
                                <?= $this->Html->link('<i class="fa fa-print fa-fw"></i>',
                                        [
                                            'controller' => 'admin',
                                            'action' => 'receiptPdf',
                                            $value['id_receipt']
                                        ],
                                        [
                                            'class'=>'pdf_report',
                                            'target'=>'_blank',
                                            'escape' => false,
                                            'link-print'
                                        ]
                                    );
                                ?></td>
                        </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </tbody>
                </table>
                <div class="col-lg-3 col-md-3 col-sm-6" data-vat>
                    <div class="row">
                        <label class="control-label">VAT</label>
                        <?=
                            $this->Form->checkbox('check_vat', [
                                    'value' => '0',
                                    'class' => 'control-label',
                                    'style' => 'margin-left: 10px;'
                                ]
                            );
                        ?>
                    </div>
                </div>
                <?php if($this->Paginator->total() > 1) : ?>
                    <div class="paginator col-lg-12">
                        <ul class="pagination pull-right cms-ul-navigation">
                            <?= $this->Paginator->first('<< ' . __('First')) ?>
                            <?= $this->Paginator->prev('< ' . __('Previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('Next') . ' >') ?>
                            <?= $this->Paginator->last(__('Last') . ' >>') ?>
                        </ul>
                        <p class="cms-margin-top-5"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                <?php endif; ?>
                <?=
                    $this->Html->link('Export', 
                        [
                            'controller' => 'admin',
                            'action' => 'receiptPdf',
                            '?' => $data_search
                        ],
                        [
                            'class'=>'pdf_report btn btn-primary pull-right',
                            'target'=>'_blank',
                            'escape' => false,
                            'btn-printer'
                        ]
                    );
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('[input-date-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('[date-picker]').click(function(){
            $('[input-date-picker]').focus();
        });

        $('[input-date-from-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        
        $('[date-from-picker]').click(function(){
            $('[input-date-from-picker]').focus();
        });

        $('[input-date-to-picker]').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('[date-to-picker]').click(function(){
            $('[input-date-to-picker]').focus();
        });
    })
       
    
</script>