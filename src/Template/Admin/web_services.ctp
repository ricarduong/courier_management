<?php
use Cake\Core\Configure;
$select_time = Configure::read('SELECT_TIME');
?>
<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h4>Quản lý dịch vụ</h4>
            
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="form-group">
            <a href="/admin/web-add-services"><input type="submit" value="Thêm dịch vụ" class="btn btn-primary"></a> 
        </div>
        <div class="panel panel-default">            
            <div class="panel-heading">
                <?= $this->Form->create('addEmployee',
                        [   'url' => [
                                'controller' => 'admin',
                                'action' => 'searchService'
                                ],
                            'class' => 'form-vertical',
                        ]
                    ); ?>
                <div class="row">                    
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="form-group">                            
                            <input type="text" class="form-control" id="email" placeholder="Nhập nội dung tìm kiếm" name="email">
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-12 col-sm-12">
                        <input type="submit" value="Search" class="btn btn-primary">                       
                    </div>
                </div>
                    <?= $this->Form->end(); ?>
            </div>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-striped table-bordered table-hover row-border bm-table-list-all">
                    <thead>
                        <tr>
                            
                            <th>Chọn</th>
                            <th>Thứ tự</th>
                            <th>Tên bài viết</th>
                            <th>Nổi bật</th>
                            <th>Ẩn/hiện</th>
                            <th>Sửa</th>
                            <th>Xóa</th>
                        </tr>
                    </thead>
                    <?php
                        if(!empty($articles)) :
                            foreach ($articles as $article =>$valuearticle):
                                if(!empty($valuearticle)) :
                    ?>                                 
                                <tr>
                                    <td></td>
                                    <td>
                                         <input type="text" value="<?=$valuearticle['stt']?>" name="ordering[]" onkeypress="return OnlyNumber(event)" class="update_stt" original-title="Nhập số thứ tự bài viết" rel="<?=$valuearticle['id']?>" />
                                    </td>
                                    <td><?=$valuearticle['name_en_US']?></td>
                                    <td>
                                        <a data-floating="<?=$valuearticle['floating']?>" class="codedaoToggle <?=($valuearticle['floating']==1)?'codedaoToggleOff':''?>" data-id="<?= $valuearticle['id'] ?>"></a>
                                    </td>
                                    <td>
                                        <a data-display="<?=$valuearticle['display']?>" class="codedaoToggle <?=($valuearticle['display']==1)?'codedaoToggleOff':''?>" data-id="<?= $valuearticle['id']?>"></a>
                                    </td>
                                    <td align="center" ><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                    <td align="center"><a href="#"><i class="fa fa-print fa-fw"></i></a></td>
                                </tr>
                    <?php
                                endif;
                            endforeach;
                      endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script(array(
    'admin',
    )) 
?>