<table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
   class="table table-bordered table-hover row-border bm-table-list-all" table-list-checkbox>
    <tbody>
        <?php 
            $total_cc_usd = 0;
            $total_cc_vnd = 0;
            $total_pp_usd = 0;
            $total_pp_vnd = 0;
            $pcs_cc = 0;
            $pcs_pp = 0; 
            $kgs_cc = 0;
            $kgs_pp = 0;
        if (!empty($isCC)): ?>
        <thead>
            <tr>
                <th>ETD</th>
                <th>H/B NO</th>
                <th>SHPR NAME</th>
                <th>CNEE NAME</th>
                <th>PCS</th>
                <th>KGS</th>
                <th>REMARK</th>
                <th>CHARGE</th>
            </tr>
        </thead>
        <?php
            foreach ($receipt_pdf as $key => $value):
                if ($value['remark'] == 'CC'):
                    $total_cc_usd = $total_cc_usd + $value['USD'];
                    $total_cc_vnd = $total_cc_vnd + $value['VND'];
                    $pcs_cc = $pcs_cc + $value['PCS'];
                    $kgs_cc = $kgs_cc + $value['KGS'];
        ?>
                <tr>
                    <td><?= date('d/m/Y', strtotime($value['date'])) ?></td>
                    <td><?= $value['HB_NO'] ?></td>
                    <td><?= $value['shipper']['shipper_name'] ?></td>
                    <td><?= $value['operator']['operator_name'] ?></td>
                    <td><?= $value['PCS'] ?></td>
                    <td><?= $value['KGS'] ?></td>
                    <td><?= $value['remark'] ?></td>
                    <td>$ <?= $value['USD'] ?></td>
                </tr>
        <?php
                endif;
            endforeach;
        ?>
        <?php endif; 

        if (!empty($isPP)) :
        ?>
            <thead>
                <tr>
                    <th>ETD</th>
                    <th>H/B NO</th>
                    <th>SHPR NAME</th>
                    <th>CNEE NAME</th>
                    <th>PCS</th>
                    <th>KGS</th>
                    <th>REMARK</th>
                    <th>CHARGE</th>
                </tr>
            </thead>
                
            <?php
                foreach ($receipt_pdf as $key => $value):
                    if ($value['remark'] == 'PP') :
                        $total_pp_usd = $total_pp_usd + $value['USD'];
                        $total_pp_vnd = $total_pp_vnd + $value['VND'];
                        $pcs_pp = $pcs_pp + $value['PCS'];
                        $kgs_pp = $kgs_pp + $value['KGS'];
            ?>
                    <tr>
                        <td><?= date('d/m/Y', strtotime($value['date'])) ?></td>
                        <td><?= $value['HB_NO'] ?></td>
                        <td><?= $value['shipper']['shipper_name'] ?></td>
                        <td><?= $value['operator']['operator_name'] ?></td>
                        <td><?= $value['PCS'] ?></td>
                        <td><?= $value['KGS'] ?></td>
                        <td><?= $value['remark'] ?></td>
                        <td>$ <?= $value['USD'] ?></td>
                    </tr>
            <?php
                    endif;
                endforeach;
            ?>
        <?php endif; ?>
        <tr>
            <td colspan="4" style="text-align: center;">TOTAL DEBIT</td>
            <td><?= $pcs_cc + $pcs_pp ?></td>
            <td><?= $kgs_cc + $kgs_pp ?></td>
            <td></td>
            <td>$ <?= $total_usd = $total_cc_usd + $total_pp_usd ?></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center;"> RATE EXCHANGE (VND)</td>
            <td colspan="2"><?= number_format($currency[0]['exchange_rate']) ?></td>
            <td colspan="2">VND <?= number_format($total_vn = $total_cc_vnd + $total_pp_vnd) ?></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;">VAT</td>
            <td><?= $percent = empty($data_search['vat']) ? '0' : '10%' ?></td>
            <td colspan="2">VND <?= number_format($vat = empty($data_search['vat']) ? 0 : ($total_vn/10)) ?></td>
        </tr>
        <tr>
            <td colspan="7" style="text-align: center;">TOTAL AMOUNT</td>
            <td>VND <?= number_format($total_vn - $vat) ?></td>
        </tr>
    </tbody>
</table>
<div class="row">
    <div class="col-lg-6 col-md-6">
        <h3>NAME: <?= $data_setting['name_vi_VN'] ?></h3>
        <h3>Account Number: <?= $data_setting['account_num'] ?></h3>
        <h3>BANK’S : <?= $data_setting['bank'] ?></h3>
        <h3>Address: <?= $data_setting['address_vi_VN'] ?></h3>
        <h4>Hotline: <?= $data_setting['hotline'] ?></h4>
    </div>
</div>
