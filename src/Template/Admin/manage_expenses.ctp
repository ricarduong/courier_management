<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h3>Quản lý các khoản chi</h3>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default fund-and-account">
            <div class="panel-heading">
                <b>Khoản chi</b>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('addExpense',
                         [  
                            'class' => 'form-vertical',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-5 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->control('name', [
                                                'type' => 'text',
                                                'label' => 'Tên Khoản chi',
                                                'class' => 'form-control',
                                                'required' => true,
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <?=
                                        $this->Form->control('note', [
                                                'type' => 'textarea',
                                                'label' => 'Ghi chú',
                                                'class' => 'form-control',
                                                'rows' => 2
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-12 col-sm-12">
                        <?=
                            $this->Form->button('Thêm', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-xs hidden-sm btn-search-payment-slip pull-right'
                            ]);
                        ?>
                        <?=
                            $this->Form->button('Thêm', [
                                'type' => 'submit',
                                'class' => 'btn btn-primary hidden-lg hidden-md pull-right'
                            ]);
                        ?>
                    </div>
                </div>
                
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>CẬP NHẬT CÁC KHOẢN CHI</b>
            </div>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-bordered table-hover row-border bm-table-list-all" list-check-box>
                    <thead>
                        <tr>
                            <th>Tên khoản chi</th>
                            <th width="50%">Ghi chú</th>
                            <th width="15%">Còn sử dụng</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!empty($data_income_expenditure)):
                                foreach ($data_income_expenditure as $key => $value):
                        ?>
                            <tr>
                                <td><?= $value['name'] ?></td>
                                <td><?= $value['note'] ?></td>
                                <td align="center">
                                    <?= $this->Form->checkbox('enabled', [
                                        'value' => $value['enabled'],
                                        'data-checked'
                                        ]); 
                                    ?>
                                </td>
                            </tr>
                        <?php
                                endforeach;
                            endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>