<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h3>Quản lý đối tượng thu chi</h3>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>Chi tiết Đối tượng</b></h4>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create('data',
                         [  
                            'class' => 'form-horizontal',
                        ]
                    );
                ?>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Mã đối tượng</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('operator_code', [
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ]
                            );
                        ?>
                        <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                    </div>
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Tên đối tượng</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('operator_name', [
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ]
                            );
                        ?>
                        <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                    </div>
                </div>
                <!-- <div class="form-group">

                </div> -->
                <div class="form-group">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Đơn vị</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('unit', [
                                    'label' => false,
                                    'class' => 'form-control'
                                ]
                            );
                        ?>
                    </div>
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Chức vụ</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('position', [
                                    'label' => false,
                                    'class' => 'form-control'
                                ]
                            );
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Địa chỉ</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('address', [
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ]
                            );
                        ?>
                        <div class='alert alert-danger ope-alert-blank hidden'>Không được để trống</div>
                    </div>
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Điện thoại</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('phone', [
                                    'type' => 'tel',
                                    'label' => false,
                                    'class' => 'form-control'
                                ]
                            );
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Fax</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('fax', [
                                    'type' => 'tel',
                                    'label' => false,
                                    'class' => 'form-control',
                                ]
                            );
                        ?>
                    </div>
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Website</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('website', [
                                    'label' => false,
                                    'class' => 'form-control'
                                ]
                            );
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Email</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('email', [
                                    'type' => 'email',
                                    'label' => false,
                                    'class' => 'form-control',
                                ]
                            );
                        ?>
                    </div>
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Số tài khoản</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('account_num', [
                                    'type' => 'tel',
                                    'label' => false,
                                    'class' => 'form-control',
                                ]
                            );
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Ngân hàng</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('bank', [
                                    'label' => false,
                                    'class' => 'form-control',
                                ]
                            );
                        ?>
                    </div>
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Loại</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->select('id_type',
                                $data_type,
                                [
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => "Chọn loại",
                                    'required' => true,
                                ]
                            );
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Mã số thuế</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('tax_code', [
                                    'type' => 'tel',
                                    'label' => false,
                                    'class' => 'form-control',
                                ]
                            );
                        ?>
                    </div>
                    <label class="control-label col-lg-2 col-md-2 col-sm-12">Ghi chú</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <?=
                            $this->Form->control('note', [
                                    'type' => 'textarea',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'rows' => 3
                                ]
                            );
                        ?>
                    </div>
                </div>
                <?=
                    $this->Form->control('Thêm', [
                            'class' => 'btn btn-primary pull-right',
                            'type' => 'submit'
                        ]
                    );
                ?>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <?= $this->element('Modal/modal_edit_expenses_object',[
        'data_operator' => $data_operator,
        'data_type'=> $data_type,
        'data'=> $data
        ]); ?>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><b>DANH SÁCH ĐỐI TƯỢNG THU CHI</b></h4>
            </div>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                    class="table table-bordered table-hover row-border bm-table-list-all">

                    <thead>
                        <tr align="center">
                            <th align="center">Loại</th>
                            <th>Mã đối tượng</th>
                            <th>Tên đối tượng</th>
                            <th>Đơn vị</th>
                            <th>Điện thoại</th>
                            <th>Địa chỉ</th>
                            <th>Sửa</th>
                        </tr>
                    </thead>
                    <?php
                        if (!empty($data)):
                            foreach ($data as $key => $value):
                    ?>
                    <tr>
                        <td><?= $value['operator_type']['type_name_ope'] ?></td>
                        <td><?= $value['operator_code'] ?></td>
                        <td><?= $value['operator_name'] ?></td>
                        <td><?= $value['unit'] ?></td>
                        <td><?= $value['phone'] ?></td>
                        <td><?= $value['address'] ?></td>
                        <th><?= $this->Form->radio('id',
                            [
                                'label' => false,
                            ],
                            [
                                'data-name-ope' => $value['operator_name'],
                                'data-address-ope' => $value['address'],
                                'data-expense-obj' => json_encode($value),
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-edit-expenses-object',
                                'data-value-ope',

                            ]
                        ); ?> </th>
                    </tr>

                    <?php
                            endforeach;
                        endif;
                    ?>
                </table>
                    <ul class="pagination" style="float:right;">
                        <?= $this->Paginator->prev('« Previous ', array('class' => 'disabled'));//Hiện thj nút Previous?>
                        <?= $this->Paginator->numbers();//Hiển thi các số phân trang?>
                        <?= $this->Paginator->next(' Next »',  array('class' => 'disabled'));//Hiển thị nút next?>
                    </ul>
            </div>
        </div>
    </div>
</div>

    <?= $this->Html->script(array(
        'admin',
        ))
    ?>
