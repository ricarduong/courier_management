<?php $this->set('title', __('msg_title_error')); ?>
<div class="row mt-4">
    <div class="col-lg-12 m-auto p-3">
        <div class="row">
            <div class="col-lg-7 m-auto">
                <h1><?= __('msg_common_error') ?></h1>
                <br>
            </div>
        </div>
    </div>
</div>