<div class="row mt-4">
    <div class="col-lg-12 m-auto p-3">
        <div class="row">
            <div class="col-lg-5 m-auto">
                <h1><?= __('msg_404_not_found') ?></h1>
                <br>
            </div>
        </div>
    </div>
</div>