<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Quản lý thu chi';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('loaders.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat" rel="stylesheet">
    <?= $this->Html->css(array(
        '/vendor_admin/bootstrap/css/bootstrap.min.css',
        '/vendor_admin/metisMenu/metisMenu.min.css',
        'sb-admin-2.css',
        '/vendor_admin/morrisjs/morris.css',
        '/vendor_admin/font-awesome/css/font-awesome.min.css',
        '/vendor/bootstrap-select/css/bootstrap-select.min.css',
        // 'selectize.css',
        'datepicker.css',
        'web_admin.css',
    )) ?>
    <!-- Javascript files-->
    <?= $this->Html->script(array(
        '/vendor_admin/jquery/jquery.min.js',        
        '/vendor_admin/bootstrap/js/bootstrap.min.js',
        '/vendor_admin/metisMenu/metisMenu.min.js',
        '/vendor_admin/raphael/raphael.min.js',
        '/vendor_admin/morrisjs/morris.min.js',
        'morris-data.js',
        'sb-admin-2.js',
        'payment_slip.js',
        'receipt.js',
        'expense_object.js',
        'expenses_revenues.js',
        'bootstrap-datepicker.js',
        '/vendor/bootstrap-select/js/bootstrap-select.min.js',
    )) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script type="text/javascript">
              $(window).on("load", function () {
                $('#page_loading').fadeOut('slow');
            });
    </script> 
    <style>
        #page_loading{
            width:100%;
            height:100%;
            position: fixed;
            top:0px;
            left: 0px;
            /*        background: #999;*/
            text-align: center;
            padding-top:25%;
            opacity: .7;
            z-index: 9999;
        }
        #page_loading div{
            
        }
        .line-scale-pulse-out-rapid > div{
            background: #F00;
        }
    </style>
    <!--  -->
</head>
<body>
    <div id="page_loading">
        <div id="icon ">
            <div class="loaded loader-inner line-scale-pulse-out-rapid">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <?= $this->element('Common/nav_top_sidebar_admin') ?>
    <div id="page-wrapper">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>

</body>
</html>
