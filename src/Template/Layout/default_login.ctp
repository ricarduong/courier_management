<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('loaders.css') ?>
    <?= $this->Html->css(array(
        '/vendor/bootstrap/css/bootstrap.min.css',
        '/vendor/font-awesome/css/font-awesome.min.css',
        '/vendor/bootstrap-select/css/bootstrap-select.min.css',
        'style.default.css',
        '/vendor/owl.carousel/assets/owl.carousel.css',
        '/vendor/owl.carousel/assets/owl.theme.default.css',
        'custom.css',
        'https://fonts.googleapis.com/css?family=Roboto:300,400,700',
        // 'selectize.css',
    )) ?>
    <!-- Javascript files-->
    <?= $this->Html->script(array(
        '/vendor/jquery/jquery.min.js',        
        '/vendor/popper.js/umd/popper.min.js',
        '/vendor/bootstrap/js/bootstrap.min.js',
        '/vendor/jquery.cookie/jquery.cookie.js',
        '/vendor/waypoints/lib/jquery.waypoints.min.js',
        '/vendor/jquery.counterup/jquery.counterup.min.js',
        '/vendor/owl.carousel/owl.carousel.min.js',
        '/vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.min.js',
        'jquery.parallax-1.1.3.js',
        '/vendor/bootstrap-select/js/bootstrap-select.min.js',
        '/vendor/jquery.scrollto/jquery.scrollTo.min.js',
        'front.js',
    )) ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script type="text/javascript">
              $(window).on("load", function () {
                $('#page_loading').fadeOut('slow');
            });
    </script> 
    <style>
        #page_loading{
            width:100%;
            height:100%;
            position: fixed;
            top:0px;
            left: 0px;
            /*        background: #999;*/
            text-align: center;
            padding-top:25%;
            opacity: .7;
            z-index: 9999;
        }
        #page_loading div{
            
        }
        .line-scale-pulse-out-rapid > div{
            background: #F00;
        }
    </style>
    <!--  -->
</head>
<body>
    <div id="page_loading">
        <div id="icon ">
            <div class="loaded loader-inner line-scale-pulse-out-rapid">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <?= $this->Flash->render() ?>
    <div class="">
        <?= $this->fetch('content') ?>
    </div>
</body>
</html>
