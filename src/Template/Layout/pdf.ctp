<?php 
use Cake\Core\Configure;
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('cake.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('home.css', ['fullBase' => true]) ?>
     <?= $this->Html->css(array(
        '/vendor_admin/bootstrap/css/bootstrap.min.css',
        '/vendor_admin/metisMenu/metisMenu.min.css',
        'sb-admin-2.css',
        '/vendor_admin/morrisjs/morris.css',
        '/vendor_admin/font-awesome/css/font-awesome.min.css',
        '/vendor/bootstrap-select/css/bootstrap-select.min.css',
        // 'selectize.css',
        'datepicker.css',
        'web_admin.css',
    )) ?>
    <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat" rel="stylesheet">
    <?= $this->Html->script(array(
        '/vendor_admin/jquery/jquery.min.js',        
        '/vendor_admin/bootstrap/js/bootstrap.min.js',
        '/vendor_admin/metisMenu/metisMenu.min.js',
        '/vendor_admin/raphael/raphael.min.js',
        '/vendor_admin/morrisjs/morris.min.js',
        'morris-data.js',
        'sb-admin-2.js',
        'payment_slip.js',
        'receipt.js',
        'expense_object.js',
        'expenses_revenues.js',
        'bootstrap-datepicker.js',
        '/vendor/bootstrap-select/js/bootstrap-select.min.js',
    )) ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body>
<header class="row">
    <?=
        $this->Form->button('Print', [
            'type' => 'button',
            'btn-printer',
            'class' => 'btn btn-primary pull-right',
            'onclick' => "this.style.display='none';window.print();window.close();return false;"
        ]);
    ?>
    <div class="header-title">
        <h2><?= $data_setting['name_vi_VN'] ?></h2>
        <h3>Address: <?= $data_setting['address_vi_VN'] ?></h3>
        <h4>Tel: <?= $data_setting['phone'] ?></h4>
        <h2>DEBIT NOTE-<?= date('m').'/'.date('Y') ?></h2>
    </div>
</header>
<div class="container">
    <div class="row">
        <?= $this->fetch('content') ?>
    </div>
</div>
</body>

</html>