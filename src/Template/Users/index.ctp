<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h4>Quản lý người dùng</h4>
            <hr>
        </div>
        
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="form-group">
            <a href="/users/add"><button type="button" class="btn btn-primary">＋ <?= __('Thêm người dùng') ?></button></a>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-striped table-bordered table-hover row-border bm-table-list-all">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Số điện thoại</th>
                            <th>Vai trò</th>
                            <th>Quyền</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <?php
                        if (!empty($data_users)):
                            foreach ($data_users as $key => $valuser):
                    ?>
                        <tr>
                            <td><?= $valuser['username'] ?></td>
                            <td><?= $valuser['phone'] ?></td>
                            <td>                    
                                <?php
                                    if (!empty($roles)):
                                        foreach ($roles as $key => $valrole):
                                            if($valrole['val'] == $valuser['role']):
                                ?>
                                <?= $valrole['name'] ?>
                                <?php
                                            endif;
                                        endforeach;
                                    endif;
                                ?>
                            </td>
                            <td>                                
                                <?php
                                    $arr_authorities_id = json_decode($valuser['authorities_id']);
                                    if (!empty($data_authorities) && !empty($arr_authorities_id)):
                                        foreach ($data_authorities as $key => $data_authoritie):
                                            foreach ($arr_authorities_id as $authorities_id):
                                                if($authorities_id == $data_authoritie['id']):
                                ?>
                                                <span class="label <?php if($data_authoritie['enable'] == 0): ?> label-danger <?php else: ?> label-success <?php endif ?>" title="<?= $data_authoritie['name'] ?>"><?= $data_authoritie['name'] ?></span>
                                <?php
                                                endif;
                                            endforeach;    
                                        endforeach;
                                    endif;
                                ?></td>
                            <td align="center"><?= $this->Html->link('<i class="fa fa-pencil"></i>',
                                ['controller' => 'Users', 'action' => 'edit', $valuser['id_user']],
                                ['escape' => false]
                                ); ?></td>
                            <td align="center"><a href="Users/delete/<?= $valuser['id_user'] ?>" onclick="return confirm('Are you sure you want to Delete?');"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>