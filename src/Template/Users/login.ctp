<?= $this->Flash->render() ?>
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-1 col-xs-10"></div>
        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 act-style-form mt-5">
            <h4 class="pt-3"><?= __('label_title_login')?></h4>
            <hr>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-10 m-auto pb-3">
                <!-- Form login -->
                <?= $this->Form->create() ?>
                    <div class="form-group">
                        <?= $this->Form->control('username', ['label' => __('label_login_username'), 'class' => 'form-control', 'autofocus' => 'true', 'maxlength' => '20', 'required' => 'required']) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('password', ['label' => __('label_password'), 'class' => 'form-control', 'type' => 'password', 'maxlength' => '100']) ?>
                    </div>
                    <button type="submit" class="btn act-btn-info btn-md float-right"><?= __('label_button_login')?></button>
                    <div class="clear"></div>
                <?= $this->Form->end() ?>
                </form>
                <!-- End form login -->
            </div>
        </div>
    </div>
</div>