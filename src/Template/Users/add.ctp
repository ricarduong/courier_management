<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h4>Thêm người dùng</h4>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-body">
              <!-- Form login -->
                <?= $this->Form->create(); ?>
                    <div class="form-group">
                        <?= $this->Form->control('username', ['label' => __('label_login_username'), 'class' => 'form-control', 'autofocus' => 'true', 'required' => 'required']) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('password', ['label' => __('label_password'), 'class' => 'form-control', 'type' => 'password',]) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('phone', ['label' => __('Phone'), 'class' => 'form-control', 'required' => 'required']) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('role', ['options' => ['0' => 'Admin', '1' => 'Staff'], 
                                                'label' => __('label_role'), 'class' => 'form-control']) ?>
                    </div>
                    <label><?= __('label_title_auth_user')?></label>
                    <div class="form-group">
                        <table class="table table-striped table-bordered table-hover row-border">
                            <thead>
                                <tr>
                                    <th> <input type="checkbox" name="check-all"> </th>
                                    <th style="align:center!important;"><?= __('label_title_auth_user')?></th>
                                </tr>
                            <tbody>
                                <?php
                                    if (!empty($data_authorities)):
                                        foreach ($data_authorities as $data_authoritie):
                                ?>
                                    <tr>
                                        <td align="center"><?= $this->Form->checkbox('authorities_id[]', ['class' => '' ,'hiddenField' => false,'id' => $data_authoritie->id , 'value' => $data_authoritie->id,]); ?></td>
                                        <td><label for=<?= $data_authoritie->id ?>><?= $data_authoritie->name ?></label></td>
                                    </tr>
                                <?php
                                        endforeach;
                                    endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn btn-primary" style="float: right;"><?= __('label_button_add_user')?></button>
                    <div class="clear"></div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>