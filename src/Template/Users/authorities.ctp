<div class="row">
    <div class="page-header-top">
        <div class="col-lg-12 col-md-12">
            <h4>Quản lý quyền</h4>
            <hr>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Thêm quyền mới</b>
            </div>
            <div class="panel-body">
                <?=
                    $this->Form->create(null,
                         [  
                            'class' => 'form-horizontal',
                        ]
                    );
                ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-2 col-md-2 control-label">Tên quyền</label>
                                    <div class="col-sm-3 col-md-3" style="padding-right: 0px;">
                                        <?=
                                            $this->Form->control('name', [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'value' => $date = empty($data['date']) ? '' : $data['date']
                                                ]
                                            );
                                        ?>
                                    </div>
                                    <label class="col-sm-2 col-md-2 control-label">Trạng thái</label>
                                    <div class="col-sm-3 col-md-3">
                                        <?= $this->Form->control('enable', ['options' => ['1' => 'Enable', '0' => 'Disable'], 
                                                'label' => false, 'class' => 'form-control']) ?>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <?=
                                            $this->Form->button('Thêm', [
                                                'type' => 'submit',
                                                'class' => 'btn btn-primary pull-right'
                                            ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-lg-12 col-md-12 bm-form-style">
        <div class="panel panel-default">
            <div class="panel-heading">
                DANH SÁCH QUYỀN
            </div>
            <div class="panel-body">
                <table id="dataTable" bm-selenium-datatype="table" bm-selenium-dataid="configDevice"
                   class="table table-striped table-bordered table-hover row-border bm-table-list-all">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Trạng Thái</th>
                            <th colspan="2">Hành động</th>
                    </thead>
                    <?php
                        if (!empty($data_authorities)):
                            foreach ($data_authorities as $key => $value):
                    ?>
                        <tr>
                            <td><?= $value['name'] ?></td>
                            <td><a data-status-authorities="<?=$value['enable']?>" class="codedaoToggle <?=($value['enable']==1)?'codedaoToggleOff':''?>" data-id-authorities="<?= $value['id'] ?>"></a></td>
                            <td align="center"><a href="#" data-toggle="modal" data-target="#modal-edit-authorities" ><i class="fa fa-pencil"></i></a></td>
                            <td align="center"><?= $this->Html->link('<i class="fa fa-trash"></i>',
                                ['controller' => 'Users', 'action' => 'deleteauthorities', $value['id']],
                                ['escape' => false],
                                ['confirm' => 'Are you sure you wish to delete this authorities?']
                                ); ?>
                            </td>
                        </tr>
                    <?php
                            echo $this->element('Modal/modal_edit_authorities',['value' => $value]);
                            endforeach;
                        endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
<?= $this->Html->script(array(
    'admin',
    )) 
?>