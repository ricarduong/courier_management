<?php

namespace App\Utility;

use Cake\Core\Configure;

class ActionCommon
{

    /**
     * @author  Dang Tam Son
     * @name    unSetItemByNullOrEmpty    
     * @todo    unset item in array if item is null or empty
     * @param   $array, $exception
     * @return  array
     * @created     2018/06/02
     * @modified    2018/06/02
     */
    public static function unSetItemByNullOrEmpty(&$array, $exception = NULL){
        foreach ($array as $key => $value){
            if(empty($exception) == FALSE && in_array($key, $exception) == TRUE) 
                continue;
            if(is_null($value) === true || $value === ""){
                unset($array[$key]);
            }
        }
    }

    /**
     * @author  Dang Tam Son
     * @name    removeSpecialCharacters    
     * @todo    remove special characters in string
     * @param   $string
     * @return  string
     * @created     2018/06/02
     * @modified    2018/06/02
     */
    public static function removeSpecialCharacters($string){
        //return trim(preg_replace('/[^A-Za-z0-9\-_]/', '', $string));
        // only replace these characters
        return preg_replace('/[!@#$%^&*()\/]/', '', $string);
    }

    /**
     * @author  Dang Tam Son
     * @name    getValueByKeyInArray    
     * @todo    after check key is exist in array or not, method will return a value
     * @param   $key, $array
     * @return  NULL | array
     * @created     2018/06/02
     * @modified    2018/06/02
     */
    public static function getValueByKeyInArray($key, &$array)
    {
        if (is_array($array) === FALSE) return NULL;
        return array_key_exists($key, $array) ? $array[$key] : NULL;
    }

}