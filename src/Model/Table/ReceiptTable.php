<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

    class ReceiptTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
            $this->belongsTo('Operator', [
                'className' => 'Operator',
                'foreignKey' => 'id_operator',
            ]);
            $this->belongsTo('Fund', [
                'className' => 'Fund',
                'foreignKey' => 'id_fund',
            ]);
            $this->belongsTo('IncomeAndExpenditure', [
                'className' => 'IncomeAndExpenditure',
                'foreignKey' => 'id_income_and_expenditure',
            ]);
            $this->belongsTo('Shipper', [
                'className' => 'Shipper',
                'foreignKey' => 'id_shipper',
            ]);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_receipt', 'A Title is required');
        }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view
     * @name    about
     * @method getReceipt()
     * @param array
     * @return
     *
     * @create  2018/06/07
    */
        public function getReceipt($data = []){
            $data_receipt = [];
            $data = $this->find('all')->contain(['operator' => [
                        'fields' => [
                            'operator.id_operator',
                            'operator.operator_name',
                            'operator.address'
                        ]
                    ],
                    'fund' => [
                        'fields' => [
                            'fund.id_fund',
                            'fund.fund_name'
                        ]
                    ],
                    'IncomeAndExpenditure' => [
                        'fields' => [
                            'IncomeAndExpenditure.id',
                            'IncomeAndExpenditure.name'
                        ]
                    ],
                    'Shipper' => [
                        'fields' => [
                            'Shipper.id_shipper',
                            'Shipper.shipper_name'
                        ]
                    ]
                ])->where(function (QueryExpression $exp) use ($data) {
                        if (!empty($data) && !empty($data['id'])) {
                            return $exp->eq('delete_flag', 0)->eq('id_receipt', $data['id']);
                        }else{
                            $query = $exp->eq('delete_flag', 0);
                        }
                        if (!empty($data['date_from']) && !empty($data['date_to'])) {
                             $query = $query->between('date', $data['date_from'], $data['date_to']);
                        }
                        if (!empty($data['date'])) {
                            $query = $query->eq('date', $data['date']);
                        }
                        if (!empty($data['fund_account'])) {
                            $query = $query->eq('fund.id_fund', $data['fund_account']);
                        }
                        if (!empty($data['remark'])) {
                            $query = $query->like('remark', '%'.$data['remark'].'%');
                        }
                        if (!empty($data['receipt_code'])) {
                            $query = $query->like('receipt_code', '%'.$data['receipt_code'].'%');
                        }
                        if (!empty($data['operator_name'])) {
                            $query = $query->like('operator.operator_name', '%'.$data['operator_name'].'%');
                        }
                        if (!empty($data['VND'])) {
                            $query = $query->like('VND', '%'.$data['VND'].'%');
                        }
                        if (!empty($data['USD'])) {
                            $query = $query->like('USD', '%'.$data['USD'].'%');
                        }
                        if (!empty($data['reason'])) {
                            $query = $query->like('reason', '%'.$data['reason'].'%');
                        }
                        if (!empty($data['HB_NO'])) {
                            $query = $query->like('HB_NO', '%'.$data['HB_NO'].'%');
                        }
                        if (!empty($data['income_name'])) {
                            $query = $query->like('IncomeAndExpenditure.name', '%'.$data['income_name'].'%');
                        }
                        return $query;
                    });
            if (empty($data))
                return $data_receipt;
            return $data;
        }

        public function lastReceipt(){
            $lastReceipt = [];
            $data = $this->find()->order(['id_receipt' => 'DESC'])->first()->toArray();
            if (empty($data))
                return $lastReceipt;
            return $data;
        }
        
    }

?>