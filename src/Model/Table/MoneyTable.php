<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

    class MoneyTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_money', 'A id is required');
        }

        public function getMoney()
        {
            $data_money = [];
            $data = $this->find('all')->toArray();
            if (empty($data))
                return $data_money;
            return $data;
        }
    }

?>