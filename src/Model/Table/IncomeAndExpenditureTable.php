<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

    class IncomeAndExpenditureTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id', 'A id is required');
        }

        public function getIncomeExpenditure($type){
            $data_income_expenditure = [];
            $data = $this->find('all')->where(['type' => $type])->toArray();
            if (empty($data))
                return $data_income_expenditure;
            return $data;
        }

        public function getFirstIncomeExpenditure($type){
            $income_expenditure = [];
            $data = $this->find()->where(['type' => $type])->order(['id' => 'DESC'])->first()->toArray();
            if (empty($data))
                return $income_expenditure;
            return $data;
        }
    }

?>