<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

    class OperatorTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
            $this->belongsTo('OperatorType', [
                'className' => 'OperatorType',
                'foreignKey' => 'id_type',
            ]);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_operator', 'A id is required');
        }

        public function getOperator()
        {
            $data_operator = [];
            $data = $this->find('all')->contain(['OperatorType' => [
                        'fields' => [
                            'OperatorType.id_type',
                            'OperatorType.type_name_ope'
                        ]
                    ]
                ])->toArray();
            if (empty($data))
                return $data_operator;
            return $data;
        }

        public function getIdOpe(){
            $id_ope = '';
            $id = $this->find()->select(['id_operator'])->order(['id_operator' => 'DESC'])->first()->toArray();
            if (empty($id))
                return $id_ope;
            return $id;
        }

        public function getOneOperator($id){
            $data_oneoperator = $this->find('all',[
                    'conditions' => ['Operator.id_operator =' => $id,
                    ]]);
            if(!empty($data_oneoperator)){
                return $data_oneoperator->toArray();
            }
            return $data_oneoperator = [];
        }

        public function searchOperator($data){
            $data_operator = $this->find('all', [
                    'conditions' => ['Operator.operator_name like' => '%'.$data['name'].'%',
                    ]])->contain(['OperatorType' => [
                        'fields' => [
                            'OperatorType.id_type',
                            'OperatorType.type_name_ope'
                        ]
                    ]
                ]);
            if(!empty($data_operator)){
                return $data_operator->toArray();
            }
            return $data_operator = [];
        }
    }

?>