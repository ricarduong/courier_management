<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

    class FundTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
            $this->belongsTo('Money', [
                'className' => 'Money',
                'foreignKey' => 'id_money',
            ]);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_fund', 'A id is required');
        }

        public function getFund()
        {
            $data_fund = [];
            $data = $this->find('all')->contain(['Money' => [
                        'fields' => [
                            'Money.id_money',
                            'Money.money_name'
                        ]
                    ]
                ])->toArray();
            if (empty($data))
                return $data_fund;
            return $data;
        }

    }

?>