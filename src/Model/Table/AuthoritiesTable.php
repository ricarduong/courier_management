<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AuthoritiesTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('name', 'A Name is required');
    }
        /**
     * @author  Dang Tam Son
     * @todo    getAuthorities
     * @name    
     * @method  
     * @param   
     * @return  
     *
     * @create  2018/06/09
     */
    public function getAuthorities(){
        $data_authorities = $this->find('all',[
                'conditions' => ['Authorities.deleted_flag =' => 0,
                ]]);
        if(!empty($data_authorities)){
            return $data_authorities->toArray();
        }
        return $data_authorities = [];
    }

}