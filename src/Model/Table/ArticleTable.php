<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArticleTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('title', 'A Title is required');
    }
        /**
     * @author  Dang Tam Son
     * @todo    get article
     * @name    getArticle 
     * @method  
     * @param   
     * @return  
     *
     * @create  2018/06/06
     */
    public function getArticle(){
        $data_article = $this->find('all');
        if(!empty($data_article)){
            return $data_article->toArray();
        }
        return $data_article = [];
    }
    
}
 ?>