<?php
namespace App\Model\Table;

use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Network\Session;

class AppTable extends Table
{

    protected $_user_data;

    public function initialize(array $options)
    {
        parent::initialize($options);

        // Get session
        $session = new Session();
        // Set global variable user data
        if(!empty($session->read('Auth.User'))){
            $this->_user_data = $session->read('Auth.User')->toArray();
        }
    }

}