<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

    class PaymentSlipTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
            $this->belongsTo('Operator', [
                'className' => 'Operator',
                'foreignKey' => 'id_operator',
            ]);
            $this->belongsTo('Fund', [
                'className' => 'Fund',
                'foreignKey' => 'id_fund',
            ]);
            $this->belongsTo('IncomeAndExpenditure', [
                'className' => 'IncomeAndExpenditure',
                'foreignKey' => 'id_income_and_expenditure',
            ]);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_payment_slip', 'A Title is required');
        }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view
     * @name    about
     * @method getPaymentSlip()
     * @param array
     * @return
     *
     * @create  2018/06/07
    */
        public function getPaymentSlip($data = []){
            $data_payment_slip = [];
            $data = $this->find('all')->contain(['operator' => [
                        'fields' => [
                            'operator.id_operator',
                            'operator.operator_name',
                            'operator.address'
                        ]
                    ],
                    'fund' => [
                        'fields' => [
                            'fund.id_fund',
                            'fund.fund_name'
                        ]
                    ],
                    'IncomeAndExpenditure' => [
                        'fields' => [
                            'IncomeAndExpenditure.id',
                            'IncomeAndExpenditure.name'
                        ]
                    ]
                ])->where(function (QueryExpression $exp) use ($data) {
                        if (!empty($data) && !empty($data['id'])) {
                            return $exp->eq('delete_flag', 0)->eq('id_payment_slip', $data['id']);
                        }else{
                            $query = $exp->eq('delete_flag', 0);
                        }
                        if (!empty($data['date_from']) && !empty($data['date_to']) && !empty($data['fund_account']) && empty($data['info'])) {
                            $query = $query->between('date', $data['date_from'], $data['date_to'])
                                           ->eq('fund.id_fund', $data['fund_account']);
                        }
                        if (!empty($data['date_from']) && !empty($data['date_to']) && empty($data['fund_account']) && empty($data['info'])) {
                             $query = $query->between('date', $data['date_from'], $data['date_to']);
                        }
                        if (empty($data['date_from']) && empty($data['date_to']) && !empty($data['fund_account']) && empty($data['info'])) {
                             $query = $query->eq('fund.id_fund', $data['fund_account']);
                        }
                        if (empty($data['date_from']) && empty($data['date_to']) && empty($data['fund_account']) && !empty($data['info'])) {
                             $query = $query->like('operator.operator_name', '%'.$data['info'].'%');
                        }
                        return $query;
                    });
            if (empty($data))
                return $data_payment_slip;
            return $data;
        }

        public function lastPaymentSlip(){
            $lastPayment = [];
            $data = $this->find()->order(['id_payment_slip' => 'DESC'])->first()->toArray();
            if (empty($data))
                return $lastPayment;
            return $data;
        }


        public function searchData($data){
            // debug($data);die;
            // $dataSearch = $this->find()->where(['id_fund =' => $data['fund_account']])->toArray();
            $dataSearch = $this->find()->where([
                'date >=' => $data['date_from'], 'date <=' => $data['date_to'], 'id_fund =' => $data['fund_account']
            ])->toArray();
            return $dataSearch;
        }
    }

?>