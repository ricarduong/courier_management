<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

    class CurrencyTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_currency', 'A id is required');
        }

        public function getCurrency()
        {
            $data_currency = [];
            $data = $this->find('all')->toArray();
            if (empty($data))
                return $data_currency;
            return $data;
        }

    }

?>