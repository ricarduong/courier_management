<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SettingTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('title', 'A Title is required');
    }
    /**
     * @author  Dang Tam Son
     * @todo    get id setting
     * @name    getSettingId 
     * @method  
     * @param   
     * @return  
     *
     * @create  2018/06/06
     */
    public function getSetting(){
        $data_setting = $this->find('all')->first();
        if(!empty($data_setting)){
            return $data_setting;
        }
        return $data_setting = [];
    }

}
 ?>