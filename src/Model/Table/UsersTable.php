<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        $role = Configure::read('ROLE');
        return $validator
            ->notEmpty('username', 'A username is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('role', 'A role is required')
            ->add('role', 'inList', [
                'rule' => ['inList', [$role[0]['val'], $role[1]['val']]],
                'message' => 'Please enter a valid role'
            ]);
    }
            /**
     * @author  Dang Tam Son
     * @todo    users
     * @name    
     * @method  
     * @param   
     * @return  
     *
     * @create  2018/06/09
     */
    public function getUsers(){
        $data_users = $this->find('all',[
                'conditions' => ['Users.deleted_flag =' => 0,
                ]]);
        if(!empty($data_users)){
            return $data_users->toArray();
        }
        return $data_users = [];
    }

}
 ?>