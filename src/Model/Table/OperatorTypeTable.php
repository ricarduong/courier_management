<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

    class OperatorTypeTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_type', 'A id is required');
        }

        public function getOperatorType(){
            $data_operator_type = [];
            $data = $this->find('all')->toArray();
            if (empty($data))
                return $data_operator_type;
            return $data;
        }
    }

?>