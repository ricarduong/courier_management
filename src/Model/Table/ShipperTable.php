<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

    class ShipperTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
        }

        public function validationDefault(Validator $validator)
        {
            return $validator
                ->notEmpty('id_fund', 'A id is required');
        }

        public function getShipper()
        {
            $data_shipper = [];
            $data = $this->find('all')->toArray();
            if (empty($data))
                return $data_shipper;
            return $data;
        }

        public function getFirstShipper(){
            $shipper = [];
            $data = $this->find()->order(['id_shipper' => 'DESC'])->first()->toArray();
            if (empty($data))
                return $shipper;
            return $data;
        }

    }

?>