<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;

class ConvertComponent extends Component
{
    public function formatDataPaymentSlip($data)
    {
        $data_payment_slip = [];
        if (empty($data)) 
            return $data_payment_slip;
        unset($data['no_bill']);
        $data['id_operator'] = $data['operator']['id_operator'];
        $data['address'] = $data['operator']['address'];
        $data['id_fund'] = $data['fund']['id_fund'];
        $date_format = str_replace('/', '-', $data['date']);
        $data['date'] = date('Y-m-d', strtotime($date_format));
        $arr_key_format['money'] = $data['money'];
        $data = $this->format_number($data, $arr_key_format);
        unset($data['operator']);
        unset($data['fund']);
        $data_payment_slip = $data;
        return $data_payment_slip;
    }

    public function formatDataReceipt($data)
    {
        $data_receipt = [];
        if (empty($data)) 
            return $data_receipt;
        unset($data['no_bill']);
        unset($data['change']);
        $data['id_operator'] = $data['operator']['id_operator'];
        $data['address'] = $data['operator']['address'];
        $data['id_fund'] = $data['fund']['id_fund'];
        $data['id_shipper'] = $data['shipper']['id_shipper'];
        $date_format = str_replace('/', '-', $data['date']);
        $data['date'] = date('Y-m-d', strtotime($date_format));
        $arr_key_format['VND'] = $data['VND'];
        $data = $this->format_number($data, $arr_key_format);
        unset($data['operator']);
        unset($data['fund']);
        unset($data['shipper']);
        $data_receipt = $data;
        return $data_receipt;
    }

    public function formatDataMortgage($data)
    {
        $data_mortgage = [];
        if (empty($data)) 
            return $data_mortgage;
        $data['id_operator'] = $data['operator']['id_operator'];
        $data['id_shipper'] = $data['shipper']['id_shipper'];
        $date_format = str_replace('/', '-', $data['date']);
        $data['date'] = date('Y-m-d', strtotime($date_format));
        $arr_key_format['value'] = $data['value'];
        $data = $this->format_number($data, $arr_key_format);
        unset($data['operator']);
        unset($data['shipper']);
        $data_mortgage = $data;
        return $data_mortgage;
    }

    public function formatOperatorType($data){
        $data_operator_type = [];
        $arr_type_id = [];
        $arr_type_name = [];
        if (empty($data)) {
            return $data_operator_type;
        }
        foreach ($data as $key => $value) {
            array_push($arr_type_id, $value['id_type']);
            array_push($arr_type_name, $value['type_name_ope']);
        }
        $data_operator_type = array_combine($arr_type_id, $arr_type_name);

        return $data_operator_type;
    }

    public function formatDataMoney($data){
        $data_money = [];
        $arr_id_money = [];
        $arr_money_name = [];
        if (empty($data)) {
            return $data_money;
        }
        foreach ($data as $key => $value) {
            array_push($arr_id_money, $value['id_money']);
            array_push($arr_money_name, $value['money_name']);
        }
        $data_money = array_combine($arr_id_money, $arr_money_name);

        return $data_money;
    }

    public function formatDataFund($data){
        $data_fund = [];
        $arr_id_fund = [];
        $arr_fund_name = [];
        if (empty($data)) {
            return $data_fund;
        }
        foreach ($data as $key => $value) {
            array_push($arr_id_fund, $value['id_fund']);
            array_push($arr_fund_name, $value['fund_name']);
        }
        $data_fund = array_combine($arr_id_fund, $arr_fund_name);

        return $data_fund;
    }

    public function formatDataShipper($data){
        $data_shipper = [];
        $arr_id_shipper = [];
        $arr_shipper_name = [];
        if (empty($data)) {
            return $data_shipper;
        }
        foreach ($data as $key => $value) {
            array_push($arr_id_shipper, $value['id_shipper']);
            array_push($arr_shipper_name, $value['shipper_name']);
        }
        $data_shipper = array_combine($arr_id_shipper, $arr_shipper_name);

        return $data_shipper;
    }

    public function formatDataIncomeExpense($data){
        $data_IE = [];
        $arr_id_IE = [];
        $arr_IE_name = [];
        if (empty($data)) {
            return $data_IE;
        }
        foreach ($data as $key => $value) {
            if ($value['enabled'] == '1') {
                array_push($arr_id_IE, $value['id']);
                array_push($arr_IE_name, $value['name']);
            }
        }
        $data_IE = array_combine($arr_id_IE, $arr_IE_name);

        return $data_IE;
    }

    public function format_number($data, $arr_key_format){
        $result = [];
        if (empty($data))
            return $result;
        if (is_array($arr_key_format)) {
            foreach ($arr_key_format as $key => $value) {
                $data[$key] = (int)str_replace(',', '', $value);
            }
        }
        return $data;
    }
    
    public function format_date($data, $arr_key_format){
        $result = [];
        if (empty($data))
            return $result;
        if (is_array($arr_key_format)) {
            foreach ($arr_key_format as $key => $value) {
                if (empty($value)) {
                    $data[$key] = '';
                }else{
                    $date_format = str_replace('/', '-', $data[$key]);
                    $data[$key] = date('Y-m-d', strtotime($date_format));
                }
            }
        }
        return $data;
    }

}