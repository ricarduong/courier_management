<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;

use Cake\I18n\I18n;
use Cake\Http\Sesion;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $helpers = ['Common', 'Configure'];

    protected $_user_data;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->_setLocaleLanguage();
        $this->isAuthorized();
        
    }
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
            'viewClassMap' => [
                'xlsx' => 'CakeExcel.Excel',
            ],
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Convert');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Admin',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
            ]
        ]);
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Security');
        $this->loadComponent('Csrf');
    }
    /**
     * @author  Dang Tam Son
     * @todo    set locale
     * @name    
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */
    private function _setLocaleLanguage()
    {
        $session = $this->request->getSession();
        $lang_default = Configure::read('Session.lang');
        $lang_change = $this->request->getQuery('lang');
        if (!empty($lang_change)) {
            $session->write('Config.language', $lang_change);
        }
        $lang_config =  $session->read('Config.language');
        if (!empty($lang_config) && $lang_config != $lang_default) {
            I18n::setLocale($lang_config);
        } else {
            $session->write('Config.language', $lang_default);
            I18n::setLocale($lang_default);
        }
    }
        /**
     * @author  Dang Tam Son
     * @name    isAuthorized    
     * @todo    after authorize doing something
     * @param   $user
     * @return  boolean
     * @created     2018/06/03
     * @modified    2018/06/03
     */
    public function isAuthorized($user = null)
    {
        // Get session
        $session = $this->request->getSession();
        // Set global variable user data
        if(!empty($session->read('Auth.User'))){
            $this->_user_data = $session->read('Auth.User');
            $this->set('_user_data', $this->_user_data);
        }

        // nothing happend
        return true;
    }
}
