<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use PHPExcel;
use PHPExcel_IOFactory;
use CakePdf;
use DOMPDF;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AdminController extends AppController
{

    public $components = array('RequestHandler');
    /**
     * @author  Dang Tam Son
     * @todo    Filter permission in controller
     * @name    beforeFilter
     * @param
     * @return  Array
     *
     * @create  2018/06/02
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Do not delete this code
        $this->viewBuilder()->setLayout('default_admin');
    }
    public $paginate = [
        'limit' => 10
    ];
    // var $paginate = array();
    var $helpers = array('PhpExcel');

    /**
     * @author  Dang Tam Son
     * @todo    view home
     * @name    index
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/02
     */
    public function index()
    {
        // echo '<pre>';
        // print_r($this->_user_data);
        // echo '</pre>';die;

    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view webServices
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */
    public function webServices()
    {
        $this->loadModel('Article');
        $articles = $this->Article->getArticle();
        $this->set(compact('articles'));
    }

        /**
     * @author  Nguyen Thai Duong
     * @todo    add webServices
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */
    public function webAddServices(){
        $langsetlang = Configure::read('LANG_LOCALE');
        $this->loadModel('Article');
        // Set data to index
        $this->set(compact('langsetlang'));
        $article = $this->Article->newEntity();
        if ($this->request->is('post')) {
            // Prior to 3.4.0 $this->request->data() was used.
            $article = $this->Article->patchEntity($article, $this->request->getData());
            if ($this->Article->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));
                return $this->redirect(['action' => 'webAddServices']);
            }
            $this->Flash->error(__('Unable to add your article.'));
        }
        $this->set('article', $article);
        
    }

    /**
     * @author  Dang Tam Son
     * @todo    change float article 
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function changeFloatArticle(){
        $this->loadModel('Article');
        if ($this->request->is(array('ajax'))) {
            $massage = '';
            $success = false;
            // Get data post
            $id = (isset($_GET["id"]) && $_GET["id"]) ? $_GET["id"] : '';
            $float_value = (isset($_GET["value"]) && $_GET["value"]) ? $_GET["value"] : '';
            // set data to save
            $post_data = [
                'floating' => $float_value,
            ];
            // Get article to edit
            $articles = $this->Article->get($id);
            // Save data to database
            $articles = $this->Article->patchEntity($articles, $post_data);
            if ($this->Article->save($articles)) {
                $success = true;
            }
            if ($success == true) {
                $massage = __('msg_data_have_been_saved');
            } elseif ($success == false) {
                $massage = __('msg_unable_to_change_data');
            } else {
                return $this->redirect(['action' => 'webServices']);
            }
        }
        // the order of these three lines is very important !!!
        $result = json_encode(array('result' => array('message' => $massage, 'success' => $success)));
        $this->response->withType('json');
        $this->response->getBody($result);

        return $this->response;
    }

        /**
     * @author  Dang Tam Son
     * @todo    change float article 
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function changeDisplayArticle(){
        $this->loadModel('Article');
        if ($this->request->is(array('ajax'))) {
            $massage = '';
            $success = false;
            // Get data post
            $id = (isset($_GET["id"]) && $_GET["id"]) ? $_GET["id"] : '';
            $display_value = (isset($_GET["value"]) && $_GET["value"]) ? $_GET["value"] : '';
            // set data to save
            $post_data = [
                'display' => $display_value,
            ];
            // Get article to edit
            $articles = $this->Article->get($id);
            // Save data to database
            $articles = $this->Article->patchEntity($articles, $post_data);
            if ($this->Article->save($articles)) {
                $success = true;
            }
            if ($success == true) {
                $massage = __('msg_data_have_been_saved');
            } elseif ($success == false) {
                $massage = __('msg_unable_to_change_data');
            } else {
                return $this->redirect(['action' => 'webServices']);
            }
        }
        // the order of these three lines is very important !!!
        $result = json_encode(array('result' => array('message' => $massage, 'success' => $success)));
        $this->response->withType('json');
        $this->response->getBody($result);

        return $this->response;
    } 

    /**
     * @author  Nguyen Thai Duong
     * @todo    view webCompanyInformation
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */
    public function webCompanyInformation()
    {
        $this->loadModel('Setting');
        $id_setting = null;
        //get data of setting
        $data_setting = $this->Setting->getSetting();
        //get language
        $langsetlang = Configure::read('LANG_LOCALE');
        if (!empty($data_setting)) {
           $id_setting =  $data_setting['id'];
        }
        // Edit data of setting
        if ($this->request->is('post')) {
            $settings = $this->Setting->get($id_setting);
            // Prior to 3.4.0 $this->request->data() was used.
            $settings = $this->Setting->patchEntity($settings, $this->request->getData());
            if ($this->Setting->save($settings)) {
                $this->Flash->success(__('Your Setting has been saved.'));
                return $this->redirect(['action' => 'webCompanyInformation']);
            }
            $this->Flash->error(__('Unable to save your setting.'));
        }
        $this->set(compact('langsetlang','data_setting'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view webContact
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */
    public function webContact()
    {
        $a = 'Thông tin website - Liên hệ';
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view webSupportCustomer
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */
    public function webSupportCustomer()
    {
        $a = 'Thông tin website - Hỗ trợ khách hàng';
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view manage revenues
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */

    public function manageRevenues()
    {
        $config_IE = Configure::read('TYPE');
        $this->loadModel('IncomeAndExpenditure');
        $data_income_expenditure = $this->IncomeAndExpenditure->getIncomeExpenditure($config_IE['I']);

        if ($this->request->is('post')) {
            $query = $this->request->getQuery();
            $data = $this->request->getData();
            $data['type'] = $config_IE['I'];
            $data['enabled'] = 1;

            $data_expense = $this->IncomeAndExpenditure->newEntity($data);
            if ($this->IncomeAndExpenditure->save($data_expense)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'manageRevenues']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }
        $this->set(compact('data_income_expenditure'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view manage Expenses
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/03
     */
    public function manageExpenses()
    {
        $config_IE = Configure::read('TYPE');
        $this->loadModel('IncomeAndExpenditure');
        $data_income_expenditure = $this->IncomeAndExpenditure->getIncomeExpenditure($config_IE['E']);

        if ($this->request->is('post')) {
            $query = $this->request->getQuery();
            $data = $this->request->getData();
            $data['type'] = $config_IE['E'];
            $data['enabled'] = 1;

            $data_expense = $this->IncomeAndExpenditure->newEntity($data);
            if ($this->IncomeAndExpenditure->save($data_expense)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'manageExpenses']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }
        $this->set(compact('data_income_expenditure'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view manage payment slip
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/15
     */
    public function managePaymentSlip()
    {
        // $inputFileName = WWW_ROOT . 'monthly.xls';
        // $spreadsheet = PHPExcel_IOFactory::load($inputFileName);
        // $sheetData = $spreadsheet->getActiveSheet(0)->toArray(null, true, true, true);
        // $sheetData = $spreadsheet->getSheet(0)->toArray(null,true,true,true);
        // foreach ($sheetData as $key => $value) {
        //     debug($value);
        // }
        // debug('abc');die;

        $config_IE = Configure::read('TYPE');
        $this->loadModel('PaymentSlip');
        $this->loadModel('Operator');
        $this->loadModel('OperatorType');
        $this->loadModel('Fund');
        $this->loadModel('IncomeAndExpenditure');
        $data_search = empty($this->request->getQuery()) ? [] : $this->request->getQuery();
        $arr_key_format['date_from'] = empty($data_search['date_from']) ? '' : $data_search['date_from'];
        $arr_key_format['date_to'] = empty($data_search['date_to']) ? '' : $data_search['date_to'];
        $query = $this->Convert->format_date($data_search, $arr_key_format);
        $data_payment_slips = $this->paginate($this->PaymentSlip->getPaymentSlip($query));
        $last_payment = $this->PaymentSlip->lastPaymentSlip();
        $data_operator = $this->Operator->getOperator();
        $data_operator_type = $this->OperatorType->getOperatorType();
        $fund = $this->Fund->getFund();
        $data_type = $this->Convert->formatOperatorType($data_operator_type);
        $data_fund = $this->Convert->formatDataFund($fund);
        $id_operator = $this->Operator->getIdOpe();
        $data_expenses = $this->IncomeAndExpenditure->getIncomeExpenditure($config_IE['E']);
        $expenses = $this->Convert->formatDataIncomeExpense($data_expenses);
        $user = $this->_user_data;
        
        if($this->request->is('post')){
            $query = $this->request->getQuery();
            $data = $this->request->getData();
            foreach ($data_payment_slips as $key => $value) {
                if ($data['payment_slip_code'] ==  $value['payment_slip_code']) {
                    $this->Flash->error('ma phieu chi da ton tai');
                    $this->set(compact('data_payment_slips','data_operator','data_type','data_fund','expenses', 'data', 'last_payment', 'user'));
                    return;
                }
            }
            $fund_remain = $this->Fund->get($data['fund']['id_fund']);
            $payment_slip = $this->Convert->formatDataPaymentSlip($data);
            $remain['remain'] = $fund_remain['remain'] - $payment_slip['money'];

            $fund_remain = $this->Fund->patchEntity($fund_remain, $remain);
            $payment_slip = $this->PaymentSlip->newEntity($payment_slip);
            if ($this->PaymentSlip->save($payment_slip) && $this->Fund->save($fund_remain)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'managePaymentSlip']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }
        $this->set(compact('data_payment_slips','data_operator','data_type','data_fund','expenses','user', 'last_payment', 'data_search'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    delete payment slip
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/09/11
     */
    public function deletePayment($id = null){
        $this->loadModel('PaymentSlip');
        // Get article to edit
        $paymentSlip = $this->PaymentSlip->get($id);
        $paymentSlip->delete_flag = 1;
        // Save data to database
        if ($this->PaymentSlip->save($paymentSlip)) {
            $this->Flash->success('Da xoa phieu chi');
            return $this->redirect(['action' => 'managePaymentSlip']);
        }else{
            $this->Flash->error('Xoa phieu chi that bai');
            return $this->redirect(['action' => 'managePaymentSlip']);
        }
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    edit payment slip
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/09/11
     */
    public function paymentEdit($id = null){
        $config_IE = Configure::read('TYPE');
        $this->loadModel('PaymentSlip');
        $this->loadModel('Operator');
        $this->loadModel('OperatorType');
        $this->loadModel('Fund');
        $this->loadModel('IncomeAndExpenditure');
        $data_operator = $this->Operator->getOperator();
        $data_operator_type = $this->OperatorType->getOperatorType();
        $fund = $this->Fund->getFund();
        $data_type = $this->Convert->formatOperatorType($data_operator_type);
        $data_fund = $this->Convert->formatDataFund($fund);
        $id_operator = $this->Operator->getIdOpe();
        $data_expenses = $this->IncomeAndExpenditure->getIncomeExpenditure($config_IE['E']);
        $expenses = $this->Convert->formatDataIncomeExpense($data_expenses);
        // Get article to edit
        $data['id'] = $id;
        $data = $this->paginate($this->PaymentSlip->getPaymentSlip($data));
        // Save data to database
        if ($this->request->is('post')) {
            $dataEdit = $this->request->getData();
            debug($dataEdit);die;
            $fund_remain = $this->Fund->get($dataEdit['fund']['id_fund']);
            $dataEdit = $this->Convert->formatDataPaymentSlip($dataEdit);
            $paymentSlip = $this->PaymentSlip->get($id);
            $remain['remain'] = $fund_remain['remain'] + $paymentSlip['money'] - $dataEdit['money'];
            // Save data to database
            $fund_remain = $this->Fund->patchEntity($fund_remain, $remain);
            $paymentSlip = $this->PaymentSlip->patchEntity($paymentSlip, $dataEdit);
            if ($this->PaymentSlip->save($paymentSlip)) {
                $this->Flash->success('Đã sửa phiếu chi');
                return $this->redirect(['action' => 'managePaymentSlip']);
            }else{
                $this->Flash->error('Sửa phiếu chi thất bại');
                return $this->redirect(['action' => 'managePaymentSlip']);
            }
        }
        
        $this->set(compact('data','data_payment_slips','data_operator','data_type','data_fund','expenses', 'user'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view receipt 
     * @name    about
     * @method manageReceipt
     * @param
     * @return
     *
     * @create  2018/09/25
     */
    public function manageReceipt($data = []){
        $config_IE = Configure::read('TYPE');
        $this->loadModel('Receipt');
        $this->loadModel('Operator');
        $this->loadModel('OperatorType');
        $this->loadModel('Fund');
        $this->loadModel('IncomeAndExpenditure');
        $this->loadModel('Shipper');
        $this->loadModel('Currency');

        $data_search = empty($this->request->getQuery()) ? [] : $this->request->getQuery();
        $arr_key_format['date_from'] = empty($data_search['date_from']) ? '' : $data_search['date_from'];
        $arr_key_format['date_to'] = empty($data_search['date_to']) ? '' : $data_search['date_to'];
        $query = $this->Convert->format_date($data_search, $arr_key_format);
        $data_receipt = $this->paginate($this->Receipt->getReceipt($query));
        $last_receipt = $this->Receipt->lastReceipt();
        $data_operator = $this->Operator->getOperator();
        $data_operator_type = $this->OperatorType->getOperatorType();
        $fund = $this->Fund->getFund();
        $data_type = $this->Convert->formatOperatorType($data_operator_type);
        $data_fund = $this->Convert->formatDataFund($fund);
        $id_operator = $this->Operator->getIdOpe();
        $data_income = $this->IncomeAndExpenditure->getIncomeExpenditure($config_IE['I']);
        $incomes = $this->Convert->formatDataIncomeExpense($data_income);
        $data_shipper = $this->Shipper->getShipper();
        $shippers = $this->Convert->formatDataShipper($data_shipper);
        $currency = $this->Currency->getCurrency();
        $user = $this->_user_data;
        if($this->request->is('post')){
            $query = $this->request->getQuery();
            $data = $this->request->getData();
            foreach ($data_receipt as $key => $value) {
                if ($data['receipt_code'] ==  $value['receipt_code']) {
                    $this->Flash->error('Mã phiếu thu đã tồn tại');
                    $this->set(compact('data_receipt','data_operator','data_type','data_fund','incomes', 'data', 'last_receipt', 'shippers', 'currency'));
                    return;
                }
            }
            $fund_remain = $this->Fund->get($data['fund']['id_fund']);
            $receipt = $this->Convert->formatDataReceipt($data);
            $remain['remain'] = $fund_remain['remain'] + $receipt['VND'];

            $fund_remain = $this->Fund->patchEntity($fund_remain, $remain);
            $receipt = $this->Receipt->newEntity($receipt);
            if ($this->Receipt->save($receipt) && $this->Fund->save($fund_remain)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'manageReceipt']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }
        $this->set(compact('data_receipt','data_operator','data_type','data_fund','incomes','user', 'last_receipt', 'shippers', 'currency', 'data_search'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    Edit receipt
     * @name    about
     * @method receiptEdit
     * @param
     * @return
     *
     * @create  2018/09/28
     */
    public function receiptEdit($id = null){
        $config_IE = Configure::read('TYPE');
        $this->loadModel('Receipt');
        $this->loadModel('Operator');
        $this->loadModel('OperatorType');
        $this->loadModel('Fund');
        $this->loadModel('IncomeAndExpenditure');
        $this->loadModel('Shipper');
        $this->loadModel('Currency');
        $data_operator = $this->Operator->getOperator();
        $data_operator_type = $this->OperatorType->getOperatorType();
        $fund = $this->Fund->getFund();
        $data_type = $this->Convert->formatOperatorType($data_operator_type);
        $data_fund = $this->Convert->formatDataFund($fund);
        $id_operator = $this->Operator->getIdOpe();
        $data_income = $this->IncomeAndExpenditure->getIncomeExpenditure($config_IE['I']);
        $incomes = $this->Convert->formatDataIncomeExpense($data_income);
        $data_shipper = $this->Shipper->getShipper();
        $shippers = $this->Convert->formatDataShipper($data_shipper);
        $currency = $this->Currency->getCurrency();
        // Get article to edit
        $data['id'] = $id;
        $data = $this->paginate($this->Receipt->getReceipt($data));
        // Save data to database
        if ($this->request->is('post')) {
            $dataEdit = $this->request->getData();
            $fund_remain = $this->Fund->get($dataEdit['fund']['id_fund']);
            $dataEdit = $this->Convert->formatDataReceipt($dataEdit);
            $receipt = $this->Receipt->get($id);
            $remain['remain'] = $fund_remain['remain'] - $receipt['VND'] + $dataEdit['VND'];
            $fund_remain = $this->Fund->patchEntity($fund_remain, $remain);
            // Save data to database
            $receipt = $this->Receipt->patchEntity($receipt, $dataEdit);
            if ($this->Receipt->save($receipt) && $this->Fund->save($fund_remain)) {
                $this->Flash->success('Đã sửa phiếu thu');
                return $this->redirect(['action' => 'manageReceipt']);
            }else{
                $this->Flash->error('Sửa phiếu thu thất bại');
                return $this->redirect(['action' => 'manageReceipt']);
            }
        }
        $this->set(compact('data_operator','data_type','data_fund','incomes','shippers', 'currency', 'data'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    delete receipt
     * @name    about
     * @method 
     * @param
     * @return
     *
     * @create  2018/09/11
     */
    public function deleteReceipt($id = null){
        $this->loadModel('Receipt');
        // Get article to edit
        $receipt = $this->Receipt->get($id);
        $receipt->delete_flag = 1;
        // Save data to database
        if ($this->Receipt->save($receipt)) {
            $this->Flash->success('Đã xóa phiếu thu');
            return $this->redirect(['action' => 'manageReceipt']);
        }else{
            $this->Flash->error('Xóa phiếu thu thất bại');
            return $this->redirect(['action' => 'manageReceipt']);
        }
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view receipt
     * @name    about
     * @method addCurrencyAjax
     * @param
     * @return
     *
     * @create  2018/09/28
     */
    public function editCurrencyAjax(){
        $this->loadModel('Currency');

        if ($this->request->is(array('ajax'))) {
            $message = '';
            $success = false;
            $data['id_currency'] = empty($_GET['id_currency']) ? '' : $_GET['id_currency'];
            $data['currency_name'] = empty($_GET['currency_name']) ? '' : $_GET['currency_name'];
            $data_format['exchange_rate'] = empty($_GET['exchange_rate']) ? '' : $_GET['exchange_rate'];
            $dataEdit = $this->Convert->format_number($data, $data_format);
            $data_currency = $this->Currency->get($data['id_currency']);
            $data_currency = $this->Currency->patchEntity($data_currency, $dataEdit);
            if ($this->Currency->save($data_currency)) {
                $success = true;
            }
            if ($success == true) {
                $message = 'Them thanh cong';
            }else{
                $message = 'that bai, ban chua nhap mot gia tri nao do';
            }
        }
        $result = json_encode(array('message' => $message, 'success' => $success, 'currency' => $data));
        die($result);
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view payment slip
     * @name    about
     * @method addOperatorAjax
     * @param
     * @return
     *
     * @create  2018/06/15
     */
    public function addOperatorAjax(){
        $this->loadModel('Operator');
        if ($this->request->is(array('ajax'))) {
            $message = '';
            $success = false;
            $data['id_operator'] = empty($_GET['id']) ? '' : $_GET['id'];
            $data['operator_code'] = empty($_GET['operator_code']) ? '' : $_GET['operator_code'];
            $data['operator_name'] = empty($_GET['operator_name']) ? '' : $_GET['operator_name'];
            $data['unit'] = empty($_GET['unit']) ? '' : $_GET['unit'];
            $data['position'] = empty($_GET['position']) ? '' : $_GET['position'];
            $data['address'] = empty($_GET['address']) ? '' : $_GET['address'];
            $data['phone'] = empty($_GET['phone']) ? '' : $_GET['phone'];
            $data['fax'] = empty($_GET['fax']) ? '' : $_GET['fax'];
            $data['website'] = empty($_GET['website']) ? '' : $_GET['website'];
            $data['email'] = empty($_GET['email']) ? '' : $_GET['email'];
            $data['account_num'] = empty($_GET['account_num']) ? '' : $_GET['account_num'];
            $data['bank'] = empty($_GET['bank']) ? '' : $_GET['bank'];
            $data['note'] = empty($_GET['note']) ? '' : $_GET['note'];
            $data['tax_code'] = empty($_GET['tax_code']) ? '' : $_GET['tax_code'];
            $data['id_type'] = empty($_GET['id_type']) ? '' : $_GET['id_type'];

            if (empty($data['id_operator'])) {
                $data_operator = $this->Operator->newEntity($data);
                $load = false;
            }else{
                $data_operator = $this->Operator->get($data['id_operator']);
                $data_operator = $this->Operator->patchEntity($data_operator, $data);
                $load = true;
            }
            if ($this->Operator->save($data_operator)) {  
                $success = true;
            }
            if ($success == true) {
                $message = 'Them thanh cong';
            }else{
                $message = 'that bai, ban chua nhap mot gia tri nao do';
            }

        }
        $id_operator = $this->Operator->getIdOpe();
        $id_ope = empty($id_operator['id_operator']) ? 1 : $id_operator['id_operator'];
        $result = json_encode(array('message' => $message, 'success' => $success, 'id_ope' => $id_ope, 'load' => $load));
        die($result);
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view payment slip
     * @name    about
     * @method addExpenseAjax
     * @param
     * @return
     *
     * @create  2018/06/15
     */
    public function addExpenseAjax(){
        $config_IE = Configure::read('TYPE');
        $this->loadModel('IncomeAndExpenditure');

        if ($this->request->is(array('ajax'))) {
            $message = '';
            $success = false;
            $data['name'] = empty($_GET['name']) ? '' : $_GET['name'];
            $data['note'] = empty($_GET['note']) ? '' : $_GET['note'];
            $data['type'] = empty($_GET['type']) ? '' : $config_IE[$_GET['type']];
            $data['enabled'] = 1;

            $data_expense = $this->IncomeAndExpenditure->newEntity($data);
            if ($this->IncomeAndExpenditure->save($data_expense)) {
                $success = true;
            }
            if ($success == true) {
                $message = 'Them thanh cong';
            }else{
                $message = 'that bai, ban chua nhap mot gia tri nao do';
            }
        }
        $data_expenses = $this->IncomeAndExpenditure->getFirstIncomeExpenditure($data['type']);
        $result = json_encode(array('message' => $message, 'success' => $success, 'expenses' => $data_expenses));
        die($result);
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view payment slip
     * @name    about
     * @method addExpenseAjax
     * @param
     * @return
     *
     * @create  2018/06/15
     */
    public function addShipperAjax(){
        $this->loadModel('Shipper');

        if ($this->request->is(array('ajax'))) {
            $message = '';
            $success = false;
            $data['shipper_name'] = empty($_GET['shipper_name']) ? '' : $_GET['shipper_name'];
            $data['address'] = empty($_GET['address']) ? '' : $_GET['address'];
            $data['phone'] = empty($_GET['phone']) ? '' : $_GET['phone'];

            $data_shipper = $this->Shipper->newEntity($data);
            if ($this->Shipper->save($data_shipper)) {
                $success = true;
            }
            if ($success == true) {
                $message = 'Them thanh cong';
            }else{
                $message = 'that bai, ban chua nhap mot gia tri nao do';
            }
        }
        $data_shipper = $this->Shipper->getFirstShipper();
        $result = json_encode(array('message' => $message, 'success' => $success, 'shipper' => $data_shipper));
        die($result);
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view fund
     * @name    about
     * @method manageFund
     * @param
     * @return
     *
     * @create  2018/06/16
     */
    public function manageFund(){
        $this->loadModel('Fund');
        $this->loadModel('Money');
        $money = $this->Money->getMoney();
        $data_fund = $this->Fund->getFund();
        $data_money = $this->Convert->formatDataMoney($money);

        if ($this->request->is('post')) {
            $query = $this->request->getQuery();
            $data = $this->request->getData();
            $arr_key_format['collected'] = $data['collected'];
            $arr_key_format['spent'] = $data['spent'];
            $arr_key_format['original_amount'] = $data['original_amount'];
            $data = $this->Convert->format_number($data, $arr_key_format);
            $data['remain'] = $data['collected'] - $data['spent'] + $data['original_amount'];
            $fund = $this->Fund->newEntity($data);
            if ($this->Fund->save($fund)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'manageFund']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }

        $this->set(compact('data_fund','data_money'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view money
     * @name    about
     * @method manageMoney
     * @param
     * @return
     *
     * @create  2018/06/16
     */
    public function manageMoney(){
        $this->loadModel('Money');
        $data_money = $this->Money->getMoney();

        if ($this->request->is('post')) {
            $query = $this->request->getQuery();
            $data = $this->request->getData();

            $money = $this->Money->newEntity($data);
            if ($this->Money->save($money)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'manageMoney']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }

        $this->set(compact('data_money'));
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view Payment Slip
     * @name    about
     * @method searchPaymentSlip
     * @param
     * @return
     *
     * @create  2018/09/27
     */
    public function searchPaymentSlip(){
        $this->loadModel('PaymentSlip');

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            return $this->redirect([
                'action' => 'managePaymentSlip',
                '?' => $data
            ]);
        }
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view Receipt
     * @name    about
     * @method searchReceipt
     * @param
     * @return
     *
     * @create  2018/09/28
     */
    public function searchReceipt(){
        $this->loadModel('Receipt');
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            return $this->redirect([
                'action' => 'manageReceipt',
                '?' => $data
            ]);
        }
    }
    
    /**
     * @author  Nguyen Duy
     * @todo    view manage Expenses Object
     * @name    about
     * @method
     * @param
     * @return
     *
     * @create  2018/07/17
     */
    public function manageExpensesObject()
    {
        $this->loadModel('Operator');
        $data_operator = $this->Operator->getOperator();
        $this->loadModel('OperatorType');
        $this->paginate = array(
            'limit' => 10,// mỗi page có 4 record
            'order' => array('id_operator' => 'asc'),
            'contain' => ['OperatorType' => [
                'fields' => [
                    'OperatorType.id_type',
                    'OperatorType.type_name_ope'
                    ]
                ]
            ]
          );
        $data = $this->paginate("Operator");
        $data_operator_type = $this->OperatorType->getOperatorType();
        $data_type = $this->Convert->formatOperatorType($data_operator_type);
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $operator = $this->Operator->newEntity($data);
            if ($this->Operator->save($operator)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'manageExpensesObject']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }
        $this->set(compact('data','data_operator','data_type'));

    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view Receipt
     * @name    about
     * @method receiptPdf
     * @param
     * @return
     *
     * @create  2018/09/28
     */
    function receiptPdf($id = null){
        $this->loadModel('Receipt');
        $this->loadModel('Currency');
        $this->loadModel('Setting');
        //get data of setting
        $data_setting = $this->Setting->getSetting();

        $data_search = empty($this->request->getQuery()) ? [] : $this->request->getQuery();
        $arr_key_format['date_from'] = empty($data_search['date_from']) ? '' : $data_search['date_from'];
        $arr_key_format['date_to'] = empty($data_search['date_to']) ? '' : $data_search['date_to'];
        $data = $this->Convert->format_date($data_search, $arr_key_format);

        $data['id'] = $id;
        $receipt_pdf = $this->paginate($this->Receipt->getReceipt($data));
        $isCC = false;
        $isPP = false;
        foreach ($receipt_pdf as $key => $value) {
            if ($value['remark'] == 'CC') {
                $isCC = true;
            }
            if ($value['remark'] == 'PP') {
                $isPP = true;
            }
        }
        $currency = $this->Currency->getCurrency();
        $this->viewBuilder()->setLayout('pdf');
        $this->viewBuilder()->getOptions([
            'pdfConfig' => [
                'orientation' => 'portrait',
                'filename' => 'agenda_123'
            ]
        ]);
        $this->set(compact('agenda','name', 'receipt_pdf', 'currency', 'data_setting', 'isCC', 'isPP', 'data_search'));

    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view Receipt
     * @name    about
     * @method paymentSlipPdf
     * @param
     * @return
     *
     * @create  2018/09/28
     */
    function paymentSlipPdf($id = null){
        $this->loadModel('PaymentSlip');
        $this->loadModel('Setting');

        $data_setting = $this->Setting->getSetting();
        $data_search = empty($this->request->getQuery()) ? [] : $this->request->getQuery();
        $arr_key_format['date_from'] = empty($data_search['date_from']) ? '' : $data_search['date_from'];
        $arr_key_format['date_to'] = empty($data_search['date_to']) ? '' : $data_search['date_to'];
        $data = $this->Convert->format_date($data_search, $arr_key_format);

        $data['id'] = $id;
        $payment_pdf = $this->paginate($this->PaymentSlip->getPaymentSlip($data));
        $agenda = 'sample agenda';
        $name = 'Nguyen Thai Duong';
        $this->viewBuilder()->setLayout('pdf');
        $this->viewBuilder()->getOptions([
            'pdfConfig' => [
                'orientation' => 'portrait',
                'filename' => 'agenda_123'
            ]
        ]);
        $this->set(compact('agenda','name', 'payment_pdf', 'data_setting'));

    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view Receipt
     * @name    about
     * @method searchOperatorAjax
     * @param
     * @return
     *
     * @create  2018/09/28
     */
    function searchOperatorAjax(){
        $this->loadModel('Operator');
        if ($this->request->is(array('ajax'))) {
            $message = '';
            $success = false;
            $html = '';
            $data['name'] = empty($_GET['name']) ? '' : $_GET['name'];
            $operator_search = $this->Operator->searchOperator($data);
            if (!empty($operator_search)) {
                $success = true;
                    foreach ($operator_search as $key => $value) {
                        $html .= '<tr>';
                            $html .= '<td>';
                                $html .= '<input type="radio" name="id" value="label" id="id-label" data-name-ope="'.$value['operator_name'].'" data-address-ope="'.$value['address'].'" data-id-ope="'.$value['id_operator'].'" data-value-ope="data-value-ope">';
                            $html .= '</td>';
                             $html .= '<td>'.$value['operator_type']['type_name_ope'].'</td>';
                             $html .= '<td>'.$value['operator_code'].'</td>';
                             $html .= '<td>'.$value['operator_name'].'</td>';
                             $html .= '<td>'.$value['unit'].'</td>';
                             $html .= '<td>'.$value['phone'].'</td>';
                             $html .= '<td>'.$value['address'].'</td>';
                        $html .= '</tr>';
                    }
            }
            if ($success == true) {
                $message = 'Them thanh cong';
            }else{
                $message = 'that bai, ban chua nhap mot gia tri nao do';
            }
        }
        $result = json_encode(array('message' => $message, 'success' => $success, 'html' => $html));
        die($result);
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view Receipt
     * @name    about
     * @method searchReceiptAjax
     * @param
     * @return
     *
     * @create  2018/09/28
     */
    public function searchReceiptAjax(){
        $this->loadModel('Receipt');
        if ($this->request->is(array('ajax'))) {
            $message = '';
            $success = false;
            $hasData = false;
            $html = '';
            $data['receipt']['income_name'] = empty($_GET['income_name']) ? '' : $_GET['income_name'];
            $data['receipt']['fund_account'] = empty($_GET['fund_account']) ? '' : $_GET['fund_account'];
            $data['receipt']['VND'] = empty($_GET['VND']) ? '' : $_GET['VND'];
            $data['receipt']['USD'] = empty($_GET['USD']) ? '' : $_GET['USD'];
            $data['receipt']['receipt_code'] = empty($_GET['receipt_code']) ? '' : $_GET['receipt_code'];
            $data['receipt']['operator_name'] = empty($_GET['operator_name']) ? '' : $_GET['operator_name'];
            $data['receipt']['reason'] = empty($_GET['reason']) ? '' : $_GET['reason'];
            $data['receipt']['HB_NO'] = empty($_GET['HB_NO']) ? '' : $_GET['HB_NO'];
            $data['receipt']['remark'] = empty($_GET['remark']) ? '' : $_GET['remark'];
            $data['receipt']['date'] = empty($_GET['date']) ? '' : $_GET['date'];
            $date_search['date'] = empty($_GET['date']) ? '' : $_GET['date'];
            $data['receipt'] = $this->Convert->format_date($data['receipt'], $date_search);
            foreach ($data['receipt'] as $key => $value) {
                if (!empty($value)) {
                    $hasData = true;
                }
            }
            if (!empty($hasData)) {
                $list_receipt = $this->paginate($this->Receipt->getReceipt($data['receipt']));
            }
            if (!empty($list_receipt)) {
                $success = true;
                    foreach ($list_receipt as $key => $value) {
                        $html .= '<tr>';
                            $html .= '<td>'; 
                                $html .= '<input type="checkbox" name="check_all" value="'.$value['collected'].'" data-checked="data-checked">';
                            $html .= '</td>';
                            $html .= '<td>'.$value['fund']['fund_name'].'</td>';
                            $html .= '<td>'.date('d/m/Y', strtotime($value['date'])).'</td>';
                            $html .= '<td>'.$value['income_and_expenditure']['name'].'</td>';
                            $html .= '<td>'.number_format($value['VND']).'</td>';
                            $html .= '<td>'.$value['USD'].'</td>';
                            $html .= '<td>'.$value['receipt_code'].'</td>';
                            $html .= '<td>'.$value['operator']['operator_name'].'</td>';
                            $html .= '<td>'.$value['reason'].'</td>';
                            $html .= '<td>'.$value['HB_NO'].'</td>';
                            $html .= '<td>'.$value['remark'].'</td>';
                            $html .= '<td><a href="/admin/receipt-edit/'.$value['id_receipt'].'"><i class="fa fa-edit"></i></a></td>';
                            $html .= '<td><a href="deleteReceipt/'.$value['id_receipt'].'"><i class="fa fa-trash"></i></a></td>';
                            $html .= '<td><a href="/admin/receipt-pdf/'.$value['id_receipt'].'" class="pdf_report" target="_blank" link-print="link-print"><i class="fa fa-print fa-fw"></i></a></td>';
                        $html .= '</tr>';
                    }
            }
            if ($success == true) {
                $message = 'Them thanh cong';
            }else{
                $message = 'that bai, ban chua nhap mot gia tri nao do';
            }
        }
        $result = json_encode(array('message' => $message, 'success' => $success, 'html' => $html));
        die($result);
    }

    /**
     * @author  Nguyen Thai Duong
     * @todo    view Mortgage
     * @name    about
     * @method manageMortgage
     * @param
     * @return
     *
     * @create  2018/10/18
     */
    public function manageMortgage(){
        $this->loadModel('Mortgage');
        $this->loadModel('Operator');
        $this->loadModel('OperatorType');
        $this->loadModel('Shipper');

        $data_search = empty($this->request->getQuery()) ? [] : $this->request->getQuery();
        $arr_key_format['date_from'] = empty($data_search['date_from']) ? '' : $data_search['date_from'];
        $arr_key_format['date_to'] = empty($data_search['date_to']) ? '' : $data_search['date_to'];
        $query = $this->Convert->format_date($data_search, $arr_key_format);
        $data_mortgage = $this->paginate($this->Mortgage->getMortgage($query));
        $data_operator = $this->Operator->getOperator();
        $data_operator_type = $this->OperatorType->getOperatorType();
        $data_type = $this->Convert->formatOperatorType($data_operator_type);
        $id_operator = $this->Operator->getIdOpe();
        $data_shipper = $this->Shipper->getShipper();
        $shippers = $this->Convert->formatDataShipper($data_shipper);

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data_mortgage = $this->Convert->formatDataMortgage($data);
            $data_mortgage = $this->Mortgage->newEntity($data_mortgage);
            if ($this->Mortgage->save($data_mortgage)) {
                $this->Flash->success('Them thanh cong');
                return $this->redirect(['action' => 'manageMortgage']);
            }else{
                $this->Flash->error('Them that bai');
                $this->set('data', $data);
            }
        }

        $this->set(compact('data_mortgage','data_operator','data_type', 'shippers', 'data_search'));
    }
}
