<?php

namespace App\Controller;
use Cake\Core\Configure;
use App\Controller\AppController;
use Cake\Event\Event;

class UsersController extends AppController
{

        /**
     * @author  Dang Tam Son
     * @todo    
     * @name    
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow('add', 'logout');
    }

        /**
     * @author  Dang Tam Son
     * @todo    
     * @name    
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
     public function index()
    {
        $roles = Configure::read('ROLE');
        $data_users = $this->Users->getUsers();
        //get data of Authorities
        $this->loadModel('Authorities');
        $data_authorities = $this->Authorities->getAuthorities();
        $this->set(compact('roles','data_authorities', 'data_users'));
        $this->viewBuilder()->setLayout('default_admin');
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name   
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function edit($id = null)
    {
        $this->loadModel('Authorities');
        $data_authorities = $this->Authorities->getAuthorities();
        $data_users = $this->Users->get($id);
        if ($this->request->is('post')) {
            // Prior to 3.4.0 $this->request->data() was used.
            $data_users = $this->Users->patchEntity($user, $this->request->getData());
            if (!empty($this->request->getData('authorities_id'))) {
               $data_users->authorities_id = json_encode($this->request->getData('authorities_id'));
            }
            if ($this->Users->save($data_users)) {
                $this->Flash->success(__('The user has been edited.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->viewBuilder()->setLayout('default_admin');
        $this->set(compact('data_users','data_authorities'));
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name    
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function add()
    {
        $this->loadModel('Authorities');
        $data_authorities = $this->Authorities->getAuthorities();
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            // Prior to 3.4.0 $this->request->data() was used.
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if (!empty($this->request->getData('authorities_id'))) {
               $user->authorities_id = json_encode($this->request->getData('authorities_id'));
            }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set(compact('user', 'data_authorities'));
        $this->viewBuilder()->setLayout('default_admin');
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name    
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function login()
    {
        $session = $this->request->getSession();
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                //write info user to session
                $session->write('Auth.User', $user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
        // Do not delete this code
        $this->viewBuilder()->setLayout('default_login');
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name   
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function delete($id = null)
    {
        $data_users = $this->Users->get($id);
        $data_users->deleted_flag = true;
        if ($this->Users->save($data_users)) {
            $this->Flash->success(__('The user has been deleted.'));
            return $this->redirect(['action' => 'index']);
        } else {
            return $this->redirect(['action' => 'index']);
            $this->Flash->error(__('Unable to delete the user.'));
        }
        // Do not delete this code
        $this->viewBuilder()->setLayout('default_login');
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name   
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function logout()
    {
        // $session = $this->request->getSession();
        //delete session when logout
        // $session->destroy();
        return $this->redirect($this->Auth->logout());
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name   
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function authorities()
    {
        $this->loadModel('Authorities');
        $data_authorities = $this->Authorities->getAuthorities();
        if ($this->request->is('post')) {
            $authorities = $this->Authorities->newEntity();
            $authorities = $this->Authorities->patchEntity($authorities, $this->request->getData());
            if ($this->Authorities->save($authorities)) {
                $this->Flash->success(__('The authorities has been saved.'));
                return $this->redirect(['action' => 'authorities']);
            }
            $this->Flash->error(__('Unable to add the authorities.'));
            return $this->redirect(['action' => 'authorities']);
        }
        $this->set(compact('data_authorities'));
        $this->viewBuilder()->setLayout('default_admin');
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name   
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function changeStatusAunthorities(){
        $this->loadModel('Authorities');
        if ($this->request->is(array('ajax'))) {
            $massage = '';
            $success = false;
            // Get data post
            $id = (isset($_GET["id"]) && $_GET["id"]) ? $_GET["id"] : '';
            $status_value = (isset($_GET["value"]) && $_GET["value"]) ? $_GET["value"] : '';
            // set data to save
            $post_data = [
                'enable' => $status_value,
            ];
            // Get article to edit
            $authorities = $this->Authorities->get($id);
            // Save data to database
            $authorities = $this->Authorities->patchEntity($authorities, $post_data);
            if ($this->Authorities->save($authorities)) {
                $success = true;
            }
            if ($success == true) {
                $massage = __('msg_data_have_been_saved');
            } elseif ($success == false) {
                $massage = __('msg_unable_to_change_data');
            } else {
                return $this->redirect(['action' => 'authorities']);
            }
        }
        // the order of these three lines is very important !!!
        $result = json_encode(array('result' => array('message' => $massage, 'success' => $success)));
        $this->response->withType('json');
        $this->response->getBody($result);

        return $this->response;
    } 

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name   
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function editAuthorities($id = null)
    {
        $this->loadModel('Authorities');
        $data_authorities = $this->Authorities->getAuthorities();
        if ($this->request->is('post')) {
            $id = $this->request->getData('id');
            if (!empty($id)) {
                $authorities = $this->Authorities->get($id);
                $authorities = $this->Authorities->patchEntity($authorities, $this->request->getData());
                if ($this->Authorities->save($authorities)) {
                    $this->Flash->success(__('The authorities has been saved.'));
                    return $this->redirect(['action' => 'authorities']);
                }
            }
            $this->Flash->error(__('Unable to add the authorities.'));
            return $this->redirect(['action' => 'authorities']);
        }
        $this->set(compact('data_authorities'));
        $this->viewBuilder()->setLayout('default_admin');
    }

    /**
     * @author  Dang Tam Son
     * @todo    
     * @name   
     * @method 
     * @param
     * @return
     *
     * @create  2018/06/11
     */
    public function deleteauthorities($id = null)
    {
        $this->loadModel('Authorities');
        $data_users = $this->Authorities->get($id);
        if ($this->Authorities->delete($data_users)) {
            $this->Flash->success(__('The authorities has been deleted.'));
            return $this->redirect(['action' => 'authorities']);
        } else {
            return $this->redirect(['action' => 'authorities']);
            $this->Flash->error(__('Unable to delete the authorities.'));
        }
        // Do not delete this code
        $this->viewBuilder()->setLayout('default_admin');
    }


}
?>