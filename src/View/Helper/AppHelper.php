<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Core\Configure;

/**
 * Application Helper
 *
 * Add your functions in the class below, your helper
 * will inherit them.
 *
 */
class AppHelper extends Helper
{
    // override the constructor if required.
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

}