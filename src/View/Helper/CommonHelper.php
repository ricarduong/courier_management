<?php

namespace App\View\Helper;

use Cake\View\View;
use Cake\Core\Configure;

use App\View\Helper\AppHelper;
use App\Utility\ActionCommon;

/**
 * @author  Dang Tam Son
 * @class   CommonHelper
 * @pospose Contain functions common for helper
 * @create  2018/06/02
 */
class CommonHelper extends AppHelper
{
    // override the constructor if required.
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /**
    * @author  Dang Tam Son
     * @name    unSetItemByNullOrEmpty    
     * @todo    unset item in array if item is null or empty
     * @param   $array, $exception
     * @return  array
     * @create      2018/06/02
     * @modified    2018/06/02
     */
    public static function unSetItemByNullOrEmpty($array, $exception = NULL)
    {
        return ActionCommon::unSetItemByNullOrEmpty($array, $exception);
    }

    /**
     * @author  Dang Tam Son
     * @name    removeSpecialCharacters    
     * @todo    remove special characters in string
     * @param   $string
     * @return  string
     * @create      2018/06/02
     * @modified    2018/06/02
     */
    public static function removeSpecialCharacters($string){
        return ActionCommon::removeSpecialCharacters($string);
    }

    /**
     * @author  Dang Tam Son
     * @name    getValueByKeyInArray    
     * @todo    after check key is exist in array or not, method will return a value
     * @param   $key, $array
     * @return  NULL | array
     * @create      2018/06/02
     * @modified    2018/06/02
     */
    public static function getValueByKeyInArray($key, $array){
        return ActionCommon::getValueByKeyInArray($key, $array);
    }

}