$(document).ready(function () {
    var paymentSlip = new PaymentSlip();
    paymentSlip.init();
    
});

function PaymentSlip(){
    var pointer = this;
    this._btn_submit_ope  = $("[btn-submit-ope]");
    this._btn_add_ope = $("[btn-add-ope]");
    this._btn_add_expense = $("[btn-add-expense]");
    
    this._data_address_ope = 'data-address-ope';
    this._data_name_ope = 'data-name-ope';
    this._data_id_ope = 'data-id-ope';
    this._id_ope = $('[id-ope=id]');
    this._name_ope = $('[name-ope=name]');
    this._data_value_ope = $("[data-value-ope]");

    this._data_input_name_ope = $("[data-input-name-ope]");
    this._data_input_address_ope = $("[data-input-address-ope]");
    this._data_input_expenses = $("[data-input-expenses]");
    this._data_json_expense = $("[data-json-expenses]");
    this._data_name_expense = $("[data-name-expenses]");

    this._data_ope_code_input = $('[data-ope-code=ope-code]');
    this._data_ope_name_input = $('[data-ope-name=ope-name]');
    this._data_ope_address_input = $('[data-ope-address=ope-address]');
    this._data_modal_operator = $('[data-modal-operator]');

    this._data_no_bill = $('[data-no-bill]');
    this._data_payment_slip_code = $('[data-payment-slip-code]');
    this._number_input = $('[number-input]');

    this._table_list_checkbox = $("[table-list-checkbox]");

    this._id_edit = $("[id-edit]");
    this._name_edit = $("[name-edit]");
    this._address_edit = $("[address-edit]");

    this._data_confirm = $("[data-confirm]");

    this._btn_search_ope = $('[btn-ope-search]');
    this._table_after_search = $('[table-after-search]');
    this._table_before_search = $('[table-before-search]');

    pointer.init = function(){
        pointer.btnEvents();
        pointer.saveDataOperator();
        pointer.addDataInput();
        pointer.eventInput();
        pointer.number_format();
        // pointer.eventCheckAll();
        // pointer.resultFlag();
        pointer.editData();
        pointer.searchOperator();
    }
    pointer.btnEvents = function(){
        pointer._btn_submit_ope.click(function() {
            pointer._data_value_ope.each(function(){
                if ($(this).prop('checked') == true) {
                    var _ope_name = $(this).attr(pointer._data_name_ope);
                    var _ope_address = $(this).attr(pointer._data_address_ope);
                    var _ope_id = $(this).attr(pointer._data_id_ope);

                    pointer._data_input_name_ope.find(pointer._id_ope).val(_ope_id);
                    pointer._data_input_name_ope.find(pointer._name_ope).val(_ope_name);
                    pointer._data_input_address_ope.find('input').val(_ope_address);
                }
            })
        })
    }

    pointer.editData = function(){
        var _ope_id = pointer._data_input_name_ope.find(pointer._id_edit).attr('id-edit');
        var _ope_name = pointer._data_input_name_ope.find(pointer._name_edit).attr('name-edit');
        var _ope_address = pointer._data_input_address_ope.find(pointer._address_edit).attr('address-edit');

        pointer._data_input_name_ope.find(pointer._id_ope).val(_ope_id);
        pointer._data_input_name_ope.find(pointer._name_ope).val(_ope_name);
        pointer._data_input_address_ope.find('input').val(_ope_address);
    }

    pointer.saveDataOperator = function(){
        pointer._btn_add_ope.click(function(){
            var data = $('form#form-data').serialize();
            if (pointer._data_ope_code_input.val() == '') {
                pointer._data_ope_code_input.closest('.form-group').find('.ope-alert-blank').removeClass('hidden');
            }else{
                pointer._data_ope_code_input.closest('.form-group').find('.ope-alert-blank').addClass('hidden');
            }
            if (pointer._data_ope_name_input.val() == '') {
                pointer._data_ope_name_input.closest('.form-group').find('.ope-alert-blank').removeClass('hidden');
            }
            else{
                pointer._data_ope_name_input.closest('.form-group').find('.ope-alert-blank').addClass('hidden');
            }
            if (pointer._data_ope_address_input.val() == '') {
                pointer._data_ope_address_input.closest('.form-group').find('.ope-alert-blank').removeClass('hidden');
            }
            else{
                pointer._data_ope_address_input.closest('.form-group').find('.ope-alert-blank').addClass('hidden');
            }
            if (pointer._data_ope_code_input.val() != '' && pointer._data_ope_name_input.val() != '' && pointer._data_ope_address_input.val() != '')
            {
                $.ajax({
                    type: 'get',
                    url: 'addOperatorAjax',
                    data: data,
                    success: function(data){
                        var notification = JSON.parse(data);

                        if(notification['success'] == true){
                            pointer.addDataInput(notification['id_ope']);
                            if (notification['load'] == true) {
                                location.reload();
                            }
                        }else{
                            pointer._data_modal_operator.find('.alert-message').removeClass('hidden');
                        }
                    }
                })
            }
        })

        pointer._btn_add_expense.click(function(){
            var data = $('form#form-data-expense').serialize();
                json_expense = pointer._data_json_expense.attr('data-json-expenses');
                name_expense = pointer._data_name_expense.val();
                arr_expense = JSON.parse(json_expense);
                exist_val = false;
                $.each(arr_expense, function(v,e){
                    if (e == name_expense) {
                        exist_val = true;
                    }
                });
            if (exist_val == true) {
                $('.data-modal-expenses').find('.alert-message').removeClass('hidden');
            }else{
                $.ajax({
                    type: 'get',
                    url: 'addExpenseAjax',
                    data: data,
                    success: function(data){
                        var notification = JSON.parse(data);
                        if (notification['success'] == true) {
                            pointer._data_input_expenses.find('select').append($('<option>', {
                                value: notification['expenses']['id'],
                                text : notification['expenses']['name']
                            })).val(notification['expenses']['id']);
                            new_expense = {[notification['expenses']['id']]:notification['expenses']['name']};
                            Object.assign(arr_expense,new_expense);
                            data_expense = JSON.stringify(arr_expense)
                            pointer._data_json_expense.attr('data-json-expenses',data_expense);
                            $('.data-modal-expenses').modal('hide');
                        }else{
                            $('.data-modal-expenses').find('.alert-message').removeClass('hidden');
                        }
                    }
                })
            }            
        })

    }
    pointer.checkInput = function(){
        name_expense = pointer._data_name_expense.val();
        return name_expense;
    }
    pointer.addDataInput = function(id){
        ope_name = pointer._data_ope_name_input.val();
        ope_address = pointer._data_ope_address_input.val();

        pointer._data_input_name_ope.find(pointer._id_ope).val(id);
        pointer._data_input_name_ope.find(pointer._name_ope).val(ope_name);
        pointer._data_input_address_ope.find('input').val(ope_address);

        $('.data-modal-operator').modal('hide');

    }

    pointer.eventInput = function(){
        var date = new Date();
            confirm = pointer._data_confirm.attr('data-confirm');
            code = '';
        year = String(date.getFullYear()).substr(2,2);
        month = date.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        if (confirm == 'E') {
            code = 'PC';
        }else{
            code = 'PT';
        }
        payment_bill = code + "/" + month + year + "/";
        if (pointer._data_no_bill.val() != '') {
            pointer._data_payment_slip_code.val(payment_bill + pointer._data_no_bill.val());
        }
        pointer._data_no_bill.on("keyup", function(){
            no_bill = $(this).val();
            payment_bill_code = payment_bill + no_bill;
            pointer._data_payment_slip_code.val(payment_bill_code);
        })
    }

    pointer.number_format = function(){
        pointer._number_input.find('input').on('keyup', function(){
            var money = $(this).val().replace(/,/g, '');
            $(this).val(money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
        })
    }

    // pointer.eventCheckAll = function(){
    //     var list_checkbox = pointer._table_list_checkbox.find('tbody');
    //     var check_all = pointer._table_list_checkbox.find('thead').find('tr');
    //     check_all.find("input[type='checkbox']").change(function(){
    //         if ($(this).is(':checked')) {
    //             $.each(list_checkbox, function(){
    //                 $(this).find("input[type='checkbox']").prop('checked', true);
    //             })
    //         }else{
    //             $.each(list_checkbox, function(){
    //                 $(this).find("input[type='checkbox']").prop('checked', false);
    //             })
    //         }
    //     })
    //     $.each(list_checkbox, function(){
    //         $(this).find("input[type='checkbox']").change(function(){
    //             if ($(this).not(':checked')) {
    //                 check_all.find("input[type='checkbox']").prop('checked', false);
    //             }
    //         })
    //     })
    // }

    // pointer.resultFlag = function(){
    //     var enabled = false;
    //     var num = 0;
    //     var check_all = pointer._table_list_checkbox.find('thead').find('tr');
    //     var list_checkbox = pointer._table_list_checkbox.find('tbody').find("input[type='checkbox']");
    //     $.each(list_checkbox, function(){
    //         if ($(this).is(':checked')) {
    //             num = num + 1;
    //         }
    //         $(this).change(function(){
    //             if ($(this).is(':checked')) {
    //                 num = num + 1;
    //             }else{
    //                 num = num - 1;
    //             }
    //             if (num > 1) {
    //                 enabled = true;
    //             }else{
    //                 enabled = false;
    //             }
    //         })
    //     })
    //     if (check_all.find("input[type='checkbox']").is(':checked')) {
    //         enabled = true;
    //     }
    // }

    pointer.searchOperator = function(){
        pointer._btn_search_ope.click(function(){
            var data = $('form#form-data-ope').serialize();
            $.ajax({
                type: 'get',
                url: 'searchOperatorAjax',
                data: data,
                success: function(data){
                    var result = JSON.parse(data);
                    if(result['success'] == true){
                        pointer._table_before_search.find('.list-operator-before-search').html(result['html']);
                        var paymentSlip = new PaymentSlip();
                        paymentSlip.init();
                    }else{
                        $('.data-modal-ope').find('.alert-message').removeClass('hidden');
                    }
                }
            });
        })
        
    }

}