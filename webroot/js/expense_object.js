$(document).ready(function () {
    var expenseObject = new ExpenseObject();
    expenseObject.init();

});

function ExpenseObject(){
    var pointer = this;
    this._edit_ope  = $("[data-value-ope]");
    this._data_input_expenses = $("[data-expense-obj]");
    this._id_ope_edit = $('[id-ope-edit]');
    this._data_ope_code = $('[data-ope-code]');
    this._data_ope_name = $('[data-ope-name]');
    this._data_ope_unit = $('[data-ope-unit]');
    this._data_ope_position = $('[data-ope-position]');
    this._data_ope_address = $('[data-ope-address]');
    this._data_ope_phone = $('[data-ope-phone]');
    this._data_ope_fax = $('[data-ope-fax]');
    this._data_ope_website = $('[data-ope-website]');
    this._data_ope_email = $('[data-ope-email]');
    this._data_ope_account_num = $('[data-ope-account-num]');
    this._data_ope_bank = $('[data-ope-bank]');
    this._data_ope_tax_code = $('[data-ope-tax-code]');
    this._data_ope_id_type = $('[data-ope-id-type]');
    pointer.init = function(){
        pointer.loadData();
    }
    pointer.loadData = function(){
        pointer._edit_ope.click(function(){
            var  jsonData = $(this).attr('data-expense-obj');
            arr_data = JSON.parse(jsonData);
            pointer._id_ope_edit.val(arr_data['id_operator']);
            pointer._data_ope_code.val(arr_data['operator_code']);
            pointer._data_ope_name.val(arr_data['operator_name']);
            pointer._data_ope_unit.val(arr_data['unit']);
            pointer._data_ope_address.val(arr_data['address']);
            pointer._data_ope_phone.val(arr_data['phone']);
            pointer._data_ope_fax.val(arr_data['fax']);
            pointer._data_ope_website.val(arr_data['website']);
            pointer._data_ope_email.val(arr_data['email']);
            pointer._data_ope_account_num.val(arr_data['account_num']);
            pointer._data_ope_bank.val(arr_data['bank']);
            pointer._data_ope_tax_code.val(arr_data['tax_code']);
            pointer._data_ope_id_type.val(arr_data['id_type']);
        });
    }

}
