$(document).ready(function () {
    var receipt = new Receipt();
    receipt.init();
});

function Receipt(){
    var pointer = this;
    this._vnd_money = $("[vnd-money]");
    this._usd_money = $("[usd-money]");
    this._change_money = $("[change-money]");
    this._number_input = $('[number-input]');
    this._data_exchange_rate = $('[data-exchange-rate]');
    this._btn_edit_currency = $('[btn-edit-currency]');
    this._btn_add_shipper = $('[btn-add-shipper]');
    this._data_json_shipper = $('[data-json-shipper]');
    this._data_name_shipper = $('[data-name-shipper]');
    this._data_input_shipper = $('[data-input-shipper]');
    this._link_print = $('[link-print]');
    this._btn_printer = $('[btn-printer]');
    this._btn_close = $('[btn-close]');
    this._checkbox_collected = $('[data-collected]');
    this._collected_check = $('[collected-check]');
    this._tr_search_element = $('[tr-search-element]');
    this._data_vat = $('[data-vat]');
    this._table_list_checkbox = $('[table-list-checkbox]');
    this._table_before_search = $('[table-before-search]');

    pointer.init = function(){
        pointer.changeMoney();
        pointer.number_format();
        pointer.editCurrency();
        pointer.addShipper();
        pointer.eventPrint();
        pointer.eventCheck();
        pointer.searchReceipt();

    }

    pointer.changeMoney = function(){
        var amount_change = pointer._change_money.val();

        pointer._usd_money.on('keyup', function(){
            var amount_usd = $(this).val();
                amount_change = pointer._change_money.val();
                amount_vnd = (amount_usd * amount_change.replace(/,/g, '')).toFixed(0);
            pointer._vnd_money.val(amount_vnd.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
        })
        pointer._vnd_money.on('keyup', function(){
            var amount_vnd = $(this).val().replace(/,/g, '');
                amount_change = pointer._change_money.val();
                amount_usd = (amount_vnd/amount_change.replace(/,/g, '')).toFixed(3);
            pointer._usd_money.val(amount_usd);
        })
    }

    pointer.exchange = function(exchange_rate){
        var amount_usd = pointer._usd_money.val();
            amount_vnd = (amount_usd * exchange_rate.replace(/,/g, '')).toFixed(0);
            pointer._vnd_money.val(amount_vnd.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    }

    pointer.number_format = function(){
        pointer._number_input.find('input').on('keyup', function(){
            var money = $(this).val().replace(/,/g, '');
            $(this).val(money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
        })
    }

    pointer.editCurrency = function(){
        pointer._btn_edit_currency.click(function(){
            var data = $('form#form-data-currency').serialize();
                exchange_rate = pointer._data_exchange_rate.val();
            $.ajax({
                type: 'get',
                url: 'editCurrencyAjax',
                data: data,
                success: function(data){
                    var notification = JSON.parse(data);
                    if (notification['success'] == true) {
                        pointer._change_money.val(exchange_rate);
                        pointer.exchange(exchange_rate);
                        $('.data-modal-currency').modal('hide');
                    }else{
                        $('.data-modal-currency').find('.alert-message').removeClass('hidden');
                    }
                }
            })
        })
    }

    pointer.addShipper = function(){
        pointer._btn_add_shipper.click(function(){
            var data = $('form#form-data-shipper').serialize();
                json_shipper = pointer._data_json_shipper.attr('data-json-shipper');
                name_shipper = pointer._data_name_shipper.val();
                arr_shipper = JSON.parse(json_shipper);
                exist_val = false;
                $.each(arr_shipper, function(v,e){
                    if (e == name_shipper) {
                        exist_val = true;
                    }
                });
            if (exist_val == true) {
                $('.data-modal-shipper').find('.alert-message').removeClass('hidden');
            }else{
                $.ajax({
                    type: 'get',
                    url: 'addShipperAjax',
                    data: data,
                    success: function(data){
                        var notification = JSON.parse(data);
                        if (notification['success'] == true) {
                            pointer._data_input_shipper.find('select').append($('<option>', {
                                value: notification['shipper']['id_shipper'],
                                text : notification['shipper']['shipper_name']
                            })).val(notification['shipper']['id_shipper']);
                            new_shipper = {[notification['shipper']['id_shipper']]:notification['shipper']['shipper_name']};
                            Object.assign(arr_shipper,new_shipper);
                            data_shipper = JSON.stringify(new_shipper)
                            pointer._data_json_shipper.attr('data-json-shipper',data_shipper);
                            $('.data-modal-shipper').modal('hide');
                        }else{
                            $('.data-modal-shipper').find('.alert-message').removeClass('hidden');
                        }
                    }
                })
            }            
        })
    }
    
    pointer.eventPrint = function(){
        // setTimeout(function(){
            pointer._btn_printer.click();
        // }, 1000);
    }

    pointer.eventCheck = function(){
        $('.form-receipt').submit(function () {
            $(this).find('input[type="checkbox"]').each( function () {
                var checkbox = $(this);
                if( checkbox.is(':checked')) {
                    checkbox.attr('value','1');
                } else {
                    checkbox.after().append(checkbox.clone().attr({type:'hidden', value:0}));
                    checkbox.prop('disabled', true);
                }
            })
        });

        var edit_check = pointer._collected_check.find(pointer._checkbox_collected).val();
        if (edit_check == 1) {
            pointer._checkbox_collected.prop('checked', true);
        }else
        {
            pointer._checkbox_collected.prop('checked', false);
        }
        var check_vat = pointer._data_vat.find("input[type='checkbox']")
        check_vat.change(function(){
            if ($(this).is(':checked')) {
                var list_print = pointer._table_list_checkbox.find(pointer._link_print);
                $.each(list_print, function(){
                    var href = $(this).attr('href');
                    $(this).attr('href', href + '?vat=1');
                })
                href_btn_exp = pointer._btn_printer.attr('href');
                if (href_btn_exp.search(/\?/) > 0) {
                    pointer._btn_printer.attr('href', href_btn_exp + '&vat=1');
                }else{
                    pointer._btn_printer.attr('href', href_btn_exp + '?vat=1');
                }
                
            }else{
                var list_print = pointer._table_list_checkbox.find(pointer._link_print);
                $.each(list_print, function(){
                    var href = $(this).attr('href');
                    $(this).attr('href', href.replace('?vat=1', ''));
                })
                href_btn_exp = pointer._btn_printer.attr('href');
                if (href_btn_exp.search(/\?vat=1/) > 0) {
                    pointer._btn_printer.attr('href', href_btn_exp.replace('?vat=1', ''));
                }else{
                    pointer._btn_printer.attr('href', href_btn_exp.replace('&vat=1', ''));
                }
            }
        })

    }

    pointer.searchReceipt = function(){
        var list_input = pointer._tr_search_element.find('.input-search');
        $.each(list_input, function(){
            $(this).on('change', function(){
                var name = $(this).attr('name');
                    value = $(this).val();
                    href = pointer._btn_printer.attr('href');
                    if (href.search(/\?/) > 0 && value != '') {
                        pointer._btn_printer.attr('href', href + '&' + name + '=' + value);
                    }else if(href.search(/\?/) < 0 && value != ''){
                        pointer._btn_printer.attr('href', href + '?' + name + '=' + value);
                    }else{
                        pointer._btn_printer.attr('href', href.replace(href, '/admin/receipt-pdf'));
                    }
                var data = $('form#form-data-search').serialize();
                $.ajax({
                    type: 'get',
                    url: 'searchReceiptAjax',
                    data: data,
                    success: function(data){
                        var result = JSON.parse(data);
                        if(result['success'] == true){
                            pointer._table_before_search.find('tbody').html(result['html']);
                            var receipt = new Receipt();
                                receipt.init();
                            var expensesRevenuse = new ExpensesRevenuse();
                                expensesRevenuse.init();
                        }else{
                            location.reload();
                        }
                    }
                });
            })
        })
    }
}