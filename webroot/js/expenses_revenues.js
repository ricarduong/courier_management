$(document).ready(function () {
    var expensesRevenuse = new ExpensesRevenuse();
    expensesRevenuse.init();
    
});

function ExpensesRevenuse(){
    var pointer = this;
    this._list_check_box = $('[list-check-box]');
    this._data_checked = $('[data-checked]');

    pointer.init = function(){
        pointer.checkEvents();
    }
    
    pointer.checkEvents = function(){
        var list_checkbox = pointer._list_check_box.find('tbody').find(pointer._data_checked);
        $.each(list_checkbox, function(){
            if ($(this).val() == 1) {
                $(this).prop('checked', true);
            }else{
                $(this).prop('checked', false);
            }
        });
    }
}