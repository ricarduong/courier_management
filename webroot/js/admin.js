$(document).ready(function(){
    var _admin = new Admin();
    _admin.ajaxEvents.changeDataFloat();
    _admin.ajaxEvents.changeDataDisplay();
    _admin.ajaxEvents.getOneOperator();
    _admin.ajaxEvents.changeDataStatusAunthorities();

});
function Admin(){
    var _pointer                            = this;
    this.ajaxEvents                         = {};

    //class
    this._class_active                      = 'codedaoToggleOff'
    //data arttribute
    this._data_floating                     = 'data-floating';
    this._data_display                      = 'data-display';
    this._data_id                           = 'data-id';
    this._data_status_authorities           = 'data-status-authorities';
    this._data_id_authorities               = 'data-id-authorities';
    this._data_id_ope                       = 'data-id-ope';



    this.ajaxEvents.changeDataFloat = function(){
        $("["+_pointer._data_floating+"]").click(function(){
            var targeturl = 'changeFloatArticle';
            if($(this).attr(_pointer._data_floating) == 0) {
                var data = {
                    id : $(this).attr(_pointer._data_id),
                    value: 1,
                }
                _pointer.ajaxEvents.saveByGetAjax(targeturl, data);
                $(this).addClass(_pointer._class_active);
                $(this).attr(_pointer._data_floating,1);   
            }else{
                var data = {
                    id : $(this).attr(_pointer._data_id),
                    value: 0,
                }
                _pointer.ajaxEvents.saveByGetAjax(targeturl, data);
                $(this).removeClass(_pointer._class_active);
                $(this).attr(_pointer._data_floating,0);
            }

        });
    }

    this.ajaxEvents.changeDataDisplay = function(){
        $("["+_pointer._data_display+"]").click(function(){
            var targeturl = 'changeDisplayArticle';
            if($(this).attr(_pointer._data_display) == 0) {
                var data = {
                    id : $(this).attr(_pointer._data_id),
                    value: 1,
                }
                _pointer.ajaxEvents.saveByGetAjax(targeturl, data);
                $(this).addClass(_pointer._class_active);
                $(this).attr(_pointer._data_display,1);   
            }else{
                var data = {
                    id : $(this).attr(_pointer._data_id),
                    value: 0,
                }
                _pointer.ajaxEvents.saveByGetAjax(targeturl, data);
                $(this).removeClass(_pointer._class_active);
                $(this).attr(_pointer._data_display,0);
            }

        });
    }

    this.ajaxEvents.changeDataStatusAunthorities = function(){
        $("["+_pointer._data_status_authorities+"]").click(function(){
            var targeturl = 'changeStatusAunthorities';
            if($(this).attr(_pointer._data_status_authorities) == 0) {
                var data = {
                    id : $(this).attr(_pointer._data_id_authorities),
                    value: 1,
                }
                _pointer.ajaxEvents.saveByGetAjax(targeturl, data);
                $(this).addClass(_pointer._class_active);
                $(this).attr(_pointer._data_status_authorities,1);   
            }else{
                var data = {
                    id : $(this).attr(_pointer._data_id_authorities),
                    value: 0,
                }
                _pointer.ajaxEvents.saveByGetAjax(targeturl, data);
                $(this).removeClass(_pointer._class_active);
                $(this).attr(_pointer._data_status_authorities,0);
            }

        });
    }

    this.ajaxEvents.saveByGetAjax = function(targeturl, data) {
        $.ajax({
            type: 'get',
            url: targeturl,
            data : data,
            beforeSend: function(xhr) 
            {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) 
            {
                // if (response.result) 
                // {
                //     var result = response.result;
                //     if (result.success == true) {
                //         window.location = href;
                //     } else {
                //         $(div_msg).removeClass('d-none');
                //         $(message).html(result.message);
                //     }
                // }
            },
            error: function(e) {
                window.location.reload();
            },
        });
    }

    this.ajaxEvents.getByGetAjax = function(targeturl, data) {
        $.ajax({
            type: 'get',
            url: targeturl,
            data : data,
            beforeSend: function(xhr) 
            {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) 
            {
                if (response.result) 
                {
                    var result = response.result;
                    if (result.success == true) {
                        // window.location = href;
                        // Console.log(response.result);
                    } else {
                        // $(div_msg).removeClass('d-none');
                        // $(message).html(result.message);
                    }
                }
            },
            error: function(e) {
                window.location.reload();
            },
        });
    }
    //ajax get id operator
    this.ajaxEvents.getOneOperator = function(){
        $("["+_pointer._data_id_ope+"]").click(function(){
            var targeturl = 'manageExpensesObject';
                var data = {
                    id : $(this).attr(_pointer._data_id_ope),
                }
                _pointer.ajaxEvents.getByGetAjax(targeturl, data);

        });
    }
}

