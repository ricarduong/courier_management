<?php
/*
 * extend configurations
 */
return [
    'LANG_LOCALE' => [
        '0'    => ['type' => 'vi_VN', 'name' => 'Tiếng Việt'],
        '1'    => ['type' => 'en_US', 'name' => 'English'],
        '2'    => ['type' => 'ko_KR', 'name' => 'Korea'],

    ],
    'SELECT_TIME' => [
        'date' => __d('manage', 'payment_slip_follow_date'),
        'week' => __d('manage', 'payment_slip_follow_week'),
        'month' => __d('manage', 'payment_slip_follow_month'),
        'year' => __d('manage', 'payment_slip_follow_year')
    ],
    'TYPE' => [
        'E' => 'C',
        'I' => 'T',
    ],
    'REMARK' => [
        'CC' => 'CC',
        'PP' => 'PP',
    ],
    'ROLE' => [
        '0'    => ['val' => '0', 'name' => 'Admin'],
        '1'    => ['val' => '1', 'name' => 'Staff'],
    ]
];